#!/bin/sh

WORKFLOW=$1
PROPERTIES=swirrl-workflow.properties
WFIMAGE_KEY="session.${WORKFLOW}.image.tag"

if [ ! -e ${PROPERTIES} ] ; then
  ## Use appending operator here, in case a parallel build wrote the file after this check
  echo "${WFIMAGE_KEY}=${SKAFFOLD_IMAGE}" >> ${PROPERTIES}
else
  if grep -q "${WFIMAGE_KEY}" ${PROPERTIES} ; then
    sed -i -e "s^${WFIMAGE_KEY}=.*^${WFIMAGE_KEY}=${SKAFFOLD_IMAGE}^" ${PROPERTIES}
  else
    echo "${WFIMAGE_KEY}=${SKAFFOLD_IMAGE}" >> ${PROPERTIES}
  fi
fi
