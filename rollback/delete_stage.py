import sys, os, shutil, os.path, time, json

"""
Example bindings in case the bindings are created from this job in the future:


bindings = {
  "var" : {
    "SystemImage" : [ {
      "@id" : "Prefix:systemimage"
    } ],
    "version" : [ "version" ],
    "endTime" : [ "1970-01-01T01:01:00+01:00" ],
    "recipeloc" : [ "recipeLocation" ],
    "rollback" : [ {
      "@id" : "Prefix:rollback"
    } ],
    "group" : [ "group" ],
    "name_api" : [ "SWIRRL-API" ],
    "startTime" : [ "1970-01-01T01:01:00+01:00" ],
    "runAgent" : [ {
      "@id" : "Prefix:runagent"
    } ],
    "Volume" : [ {
      "@id" : "Prefix:volume"
    } ],
    "method_path" : [ "DEL /session/{id}/lateststage" ],
    "K8Srecipe" : [ {
      "@id" : "Prefix:K8srecipe"
    } ],
    "sessionId" : [ "session id 1234" ],
    "authmode" : [ {
        "@id": "uuid:auth_mode_id"
    } ],
    "jobId" : [ "job id 1234" ],
    "systemImageReference" : [ "system image reference" ],
    "user" : [ {
      "@id" : "prefix:username"
    } ],
    "name" : [ "Name" ]
  },
  "vargen" : { },
  "context" : {
      "swirrl": "http://swirrl.knmi.nl/ns#",
      "uuid": "urn:uuid:"
  }
}
"""

staging_history_path = "/data-dir/staginghistory"
latest_path = "/data-dir/latest"
fileinfo_json = "swirrl_fileinfo.json"
configMap = '/data/inputs/metadata.txt'


def main():
    delete_latest_stage()

def update_symlink(targetStageNumber):
    '''
    Update symlink with the new stage. Python doesn't allow overwriting links for folders (as ln -sfn does).
    '''
    try:
        os.chdir("/data-dir/")
        os.remove('latest')
        dirPaths = [f.path for f in os.scandir(staging_history_path) if os.path.isdir(f) and targetStageNumber not in f.path]
        lastAvailableStage = dirPaths[-1].split("/")[-1]

        os.symlink("staginghistory/{}".format(lastAvailableStage), 'latest', target_is_directory = True)

    except OSError as e:
        print ('There was an issue replacing the symlink for "latest" - {}'.format(e))

def get_stage_info(folder):
    path =  folder + "/" + fileinfo_json

    with open(path) as jsonfile:
        stageInfo = json.loads(jsonfile.read())
        return stageInfo

def get_stage_dict(folder):
    path =  folder + "/" + fileinfo_json

    with open(path) as jsonfile:
        stageInfo = json.loads(jsonfile.read())
        return {"runId": stageInfo["runId"], "stageNumber" : stageInfo["stageNumber"]}
      
def determine_last_avail_stage(runIds):
    if not os.path.exists(staging_history_path): return ""

    folders = [f.path for f in os.scandir(staging_history_path) if os.path.isdir(f)]
    stages = [get_stage_dict(folder) for folder in folders if len(folders) > 0]

    if len(runIds) == 1 and runIds[0] == stages[0]["runId"]:
        print("Only one stage found: {}".format(stages[0]))
        return stages[0]

    else: 
        runs_to_process = runIds
        while len(runs_to_process) > 0:
            lastId = runs_to_process.pop()
            for stage in stages:
                if stage["runId"] == lastId:
                    print("returning stage: {}".format(stage))
                    return stage
                else:
                  continue
        return None


def delete_latest_stage():
    '''
    Delete the latest stage from the staging history or pass if there aren't any.
    '''

    try:
        with open(configMap) as reader:
            runIdString = reader.read().replace('\n', '')
            runIds = runIdString.replace("[","").replace("]", "").replace(" ", "").split(",")
            print("Read runid from configmap: {}".format(runIds))
            if not runIds: return 1
    except FileNotFoundError:
        print("Could not read runId from configmap")

    last_stage = determine_last_avail_stage(runIds)

    if not "stageNumber" in last_stage or not "runId" in last_stage :
        print("No matching stage found for any of these ids: {}".format(runIds))
        return 1

    target_stage_number = last_stage["stageNumber"]
    target_run_id = last_stage["runId"]
    stage_to_delete = "stage."+target_stage_number

    dirs = None
    if os.path.exists(staging_history_path):
        dirs = os.listdir(staging_history_path)
    else: return 1

    if dirs and len(dirs) > 1:
        # We only update the symlink if really the last stage should be deleted:
        if get_stage_info(latest_path)["runId"] == target_run_id:
            latest_stage = os.readlink(latest_path)
            print("Latest stage has id {} ", format(latest_stage))
            try:
                update_symlink(target_stage_number)

            except OSError as error:
                raise Exception("Could not update symlink for %s from staging history, %s"%(latest_stage, error))

        # Delete the stage folder in any case:
        try:
            shutil.rmtree(staging_history_path + "/" + stage_to_delete)
            print("stage {} from run {} deleted from staging history".format(target_stage_number, target_run_id))
            return
        except OSError as error:
            raise Exception("Could not delete %s from staging history, %s"%(latest_stage, error))

    if dirs and len(dirs) == 1:
        if get_stage_info(latest_path)["runId"] == target_run_id:
            try:
                os.remove(latest_path)
                shutil.rmtree(staging_history_path)
                print("stage {} from run {} deleted from staging history".format(target_stage_number, target_run_id))
                return

            except OSError as error:
                raise Exception("Could not remove last stage from staging history, %s"%(error))

    else:
        print("Could not delete from staging history - no stage found or runId not matching")
    return 1

if __name__ == "__main__":
    rc = delete_latest_stage()
    sys.exit(rc)