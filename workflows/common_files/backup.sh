#!/bin/bash

# Determine location of this script. Pythonscript backup.py is located in same path
scriptpath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
python3 "${scriptpath}/backup.py" "$@"
