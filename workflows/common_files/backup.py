#!/usr/bin/env python3
import os
import sys
import filecmp
import shutil
from datetime import datetime
import json
import uuid
import re

from os import listdir
from os.path import isfile, join

URL_FILE_EXTENSION = ".__url__"
STAGE_META_FILENAME = "swirrl_fileinfo.json" 
STAGE_N_KEY = 'stageNumber'
STAGE_RUN_KEY = 'runId'
STAGE_PATH_KEY = 'stagePath'
STAGE_PATH_PREFIX = '~/data/staginghistory/'          
STAGE_TIME_KEY = 'stageCreateTime'
FILES_KEY = 'files'
FILENAME_KEY = 'filename'
FILE_ID_KEY = 'id'
FILE_PREV_ID_KEY = 'prevId'
FILE_SOURCE_KEY = 'sourceUrl'
FILE_STATE_KEY = 'state'
FILE_STATE_NOT_EXIST = 'does_not_exist'
FILE_STATE_CHANGED = 'updated'
FILE_STATE_UNCHANGED = 'unchanged'
FILE_STATE_NEW = 'new'





# Common functions
def pathclose(path):
    return os.path.join(path, '')

def hasextension(pathname, extension):
    return pathname[-1*len(extension):] == extension

    
def intsafe(curstr,whenfail = 0):
    resval=''

    for ch in curstr:
        if(ch=='-'):
            if(resval==''):
                resval = ch
        elif(ch.isdigit()):
            resval += ch

    if(resval==''):
        resval=whenfail
    else:
        try:
            resval=int(resval)
        except:
            resval=whenfail

    return resval

def hardcopytree(srcpathname,trgpathname):
    # Python3 version: shutil.copytree(srcpathname,trgpathname,copy_function=os.link)
    workpath = os.getcwd()
    os.chdir(srcpathname)
    os.mkdir(trgpathname)
    for rootpath,dirnames,filenames in os.walk('.'):
        curtrgpath = os.path.join(trgpathname,rootpath)
        for dirname in dirnames:
            os.mkdir(os.path.join(curtrgpath,dirname))
        for filename in filenames:
            srcfilename=os.path.join(rootpath,filename)
            trgfilename=os.path.join(curtrgpath,filename)
            os.link(srcfilename,trgfilename)
    os.chdir(workpath)

def addsourceurl(file_info):
    for fileorig in sys.argv[2:]:
        if hasextension(fileorig, URL_FILE_EXTENSION):
            if file_info[FILENAME_KEY] == os.path.basename(fileorig)[:-len(URL_FILE_EXTENSION)]:
                with open(fileorig, 'r') as infile :
                   file_info[FILE_SOURCE_KEY] = infile.read().rstrip()

def get_fileid(filename, stagemeta):
    if FILES_KEY in stagemeta.keys():
        for file_info in stagemeta[FILES_KEY]:
            if FILENAME_KEY in file_info.keys() \
                and filename == file_info[FILENAME_KEY] \
                and FILE_ID_KEY in file_info.keys():
                return file_info[FILE_ID_KEY]
    return None

def get_filesource(filename, stagemeta):
    if FILES_KEY in stagemeta.keys():
        for file_info in stagemeta[FILES_KEY]:
            if FILENAME_KEY in file_info.keys() \
                and filename == file_info[FILENAME_KEY] \
                and FILE_SOURCE_KEY in file_info.keys():
                return file_info[FILE_SOURCE_KEY]
    return None

def get_runId():
    datapath = '/data/inputs/metadata.txt'

    with open(datapath) as reader:
        runId = reader.read().replace('\n', '')
        return runId

# Settings

print("************** debug print argv")
print(sys.argv)


if(len(sys.argv)<3):
    print("Usage: "+sys.argv[0]+" [path to backup to] [file 1 to backup] [file 2 to backup] [..etc..] [URL_file 1] [URL_file 2] [..etc..]")
    print("Too few arguments specified")
    sys.exit(-1)

compareshallow=False
pathbackup=pathclose(sys.argv[1])
filebackuphist=pathbackup+"backuphist.txt"

# - Find previous backup
dirbackuplast=''
backupnewnum=1
if(not os.path.exists(pathbackup)):
    os.makedirs(pathbackup)
for diritem in sorted(os.listdir(pathbackup)):
    if(diritem.startswith('stage.') and (os.path.isdir(os.path.join(pathbackup,diritem)))):
        backupcurnum=intsafe(diritem)
        if(backupcurnum>=backupnewnum):
            dirbackuplast=diritem
            backupnewnum=backupcurnum+1


dirbackupnew="stage."+str(backupnewnum).zfill(5)+"/"
pathbackupnew=pathbackup+dirbackupnew
pathbackuplast=''

# Initialize Stage metadata
stagemetanew = {}
stagemetanew[STAGE_N_KEY] = str(backupnewnum).zfill(5)
# Create a meaningfull stage path for a Jupyter service user. 
stagemetanew[STAGE_RUN_KEY] = get_runId()
stagemetanew[STAGE_PATH_KEY] = STAGE_PATH_PREFIX + dirbackupnew 
stagemetanew[STAGE_TIME_KEY] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
stagemetanew[FILES_KEY] = []
stagemetaold = {}

# Execution
if(dirbackuplast==''):
    print("Analyse:    First backup ever")
    if(not os.path.exists(pathbackupnew)):
        os.makedirs(pathbackupnew)
else:
    pathbackuplast=pathclose(pathbackup+dirbackuplast)
    print("Analyse:    Previous backup path: "+pathbackuplast)
    hardcopytree(pathbackuplast,pathbackupnew)

    stagemetaoldfilename = os.path.join(pathbackuplast, STAGE_META_FILENAME)
    if (os.path.isfile(stagemetaoldfilename)):
        with open(stagemetaoldfilename) as infile:
            stagemetaold = json.load(infile)

    if isinstance(stagemetaold[FILES_KEY], dict):
        print("*** Ignoring {}, it contains the old format.".format(stagemetaoldfilename))
        stagemetaold = {}

for fileorig in sys.argv[2:]:
    if hasextension(fileorig, URL_FILE_EXTENSION):
        continue
    
    filename=os.path.basename(fileorig)
    filenew=pathbackupnew+filename
    file_info = {FILENAME_KEY:filename}
    file_info[FILE_STATE_KEY] = file_info[FILE_ID_KEY] = file_info[FILE_SOURCE_KEY] = "UNKNOWN"

    if(os.path.isfile(fileorig)==False):
        print("Error:  Specified file does not exist: \""+fileorig+"\"")
        file_info[FILE_STATE_KEY] = FILE_STATE_NOT_EXIST
        addsourceurl(file_info)
        stagemetanew[FILES_KEY].append(file_info)
        continue

    if(dirbackuplast!=''):
        filelast=pathbackuplast+filename
        if(os.path.isfile(filelast)):
            if(filecmp.cmp(fileorig,filelast,shallow=compareshallow)):
                print("Hardlinking \""+filenew+"\"\n       from \""+filelast+"\"")
                
                file_info[FILE_ID_KEY] = get_fileid(filename, stagemetaold)
                file_info[FILE_SOURCE_KEY] = get_filesource(filename, stagemetaold)
                file_info[FILE_STATE_KEY] = FILE_STATE_UNCHANGED
                stagemetanew[FILES_KEY].append(file_info) 
                continue

    file_info[FILE_ID_KEY] = str(uuid.uuid4())

    if(os.path.isfile(filenew)):
        print("Overwriting \""+filenew+"\"\n       from \""+fileorig+"\"")
        
        file_info[FILE_STATE_KEY] = FILE_STATE_CHANGED
        # Add the ID of the previous version to
        file_info[FILE_PREV_ID_KEY] = get_fileid(filename, stagemetaold)
        os.remove(filenew)
    else:
        print("Copying     \""+filenew+"\"\n       from \""+fileorig+"\"")
        file_info[FILE_STATE_KEY] = FILE_STATE_NEW
    shutil.copy2(fileorig,filenew)

    addsourceurl(file_info)
    stagemetanew[FILES_KEY].append(file_info) 

# Add existing files to stage meta
if FILES_KEY in stagemetaold.keys():
    for old_file_info in stagemetaold[FILES_KEY]:
        file_present_old = False
        for new_file_info in stagemetanew[FILES_KEY]:
            if old_file_info[FILENAME_KEY] == new_file_info[FILENAME_KEY]:
                file_present_old = True
        if not file_present_old:
            old_file_info[FILE_STATE_KEY] = FILE_STATE_UNCHANGED
            stagemetanew[FILES_KEY].append(old_file_info)

# Write the stage meta file. This file could be hardlinked, so should be deleted first
stagemetafilepath = os.path.join(pathbackupnew,STAGE_META_FILENAME)
if os.path.isfile(stagemetafilepath): os.remove(stagemetafilepath)
with open(stagemetafilepath, 'w') as outfile:
    json.dump(stagemetanew, outfile, indent=4)
print(json.dumps(stagemetanew, indent=4))

# Recreate the 'latest' link. It should point to the staginghistory/stage.xxxx
# directory we just created. The current working directory should disappear when
# the container stops.
curdir = os.getcwd()
os.chdir(os.path.split(pathbackup.rstrip('/'))[0])
# Create a temporary link, because we cannot relink an existing symlink.
os.symlink('/'.join(pathbackupnew.rstrip('/').lstrip('/').split('/')[-2:]), "latest.tmp")
# Rename the temporary link, overwriting the old one.
os.rename("latest.tmp", "latest")
os.chdir(curdir)
print("Backup finished.")
