#!/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
cd ${SCRIPTPATH} || { echo "could not cd to ${SCRIPTPATH}"; exit 1; }
rm -rf ${SCRIPTPATH}/result/
mkdir result
cd result

STAGE_META_FILENAME="swirrl_fileinfo.json"
created_meta="${SCRIPTPATH}/result/stage.00005/${STAGE_META_FILENAME}"
previous_meta="${SCRIPTPATH}/result/stage.00004/${STAGE_META_FILENAME}"

function get_meta_file_info(){
  # Get info for filename $1 from current and previous STAGE_META_FILENAME
  created_stagenum=$(jq -r '.stageNumber' "${created_meta}")
  previous_stagenum=$(jq -r '.stageNumber' "${previous_meta}")
  created_stagepath=$(jq -r '.stagePath' "${created_meta}")
  
  created_filenames=( $(jq -r '.files[].filename' ${created_meta}) )
  created_ids=( $(jq -r '.files[].id' ${created_meta}) )
  created_states=( $(jq -r '.files[].state' ${created_meta}) )
  created_sources=( $(jq -r '.files[].sourceUrl' ${created_meta}) )
  previous_filenames=( $(jq -r '.files[].filename' ${previous_meta}) )
  previous_ids=( $(jq -r '.files[].id' ${previous_meta}) )

  for n in ${!created_filenames[*]} ; do
    if [ "${created_filenames[n]}" = "$1" ] ; then
     created_id=${created_ids[n]}
     created_state=${created_states[n]}
     created_source=${created_sources[n]}
     created_previd=$(jq -r --arg N ${n} '.files[$N | tonumber].prevId' ${created_meta})
    fi
  done 

  for n in ${!previous_filenames[*]} ; do
    if [ "${previous_filenames[n]}" = "$1" ] ; then
      previous_id=${previous_ids[n]}
    fi
  done
}

function cleanup () {
  [ -d ${SCRIPTPATH}/result/ ] && rm -rf ${SCRIPTPATH}/result/
  [ -h ${SCRIPTPATH}/latest ] && rm ${SCRIPTPATH}/latest
}

function assert_equal () {
  # $1 First term
  # $2 Second term
  # $3 Error message
  if [ "${1}" != "${2}" ] ; then
    echo "*** Error: Test Failed! ${3}"
    cleanup
    exit 1
  fi
}

function assert_not_equal () {
  # $1 First term
  # $2 Second term
  # $3 Error message
  if [ "${1}" = "${2}" ] ; then
    echo "*** Error: Test Failed! ${3}"
    cleanup
    exit 1
  fi
}

function assert_not_empty () {
  # $1 First term
  # $2 Error message
  if [ -z "${1}" ] ; then
    echo "*** Error: Test Failed! ${2}"
    cleanup
    exit 1
  fi
}

# Prepare test set-up of file in different statusses
mkdir Ext
mkdir In
mkdir -p stage.00004
mkdir -p stage.00001

# A file in the stage that should be persistent throughout backups, hard-linked across stages.
echo "This is an file that existed in previous backups" > stage.00001/file_persistent_in_backup.txt
ln ${SCRIPTPATH}/result/stage.00001/file_persistent_in_backup.txt stage.00004/file_persistent_in_backup.txt

# Backupscript should make a hardlink of this file.. even though the Input file is of newer date
echo "This is a symlink file at the source, and existed in previous backups stage.0001 and stage.0004" > Ext/file_symlink_previously_backuped.txt
ln -s ${SCRIPTPATH}/result/Ext/file_symlink_previously_backuped.txt In/file_symlink_previously_backuped.txt
cp In/file_symlink_previously_backuped.txt stage.00001/file_symlink_previously_backuped.txt
ln stage.00001/file_symlink_previously_backuped.txt stage.00004/file_symlink_previously_backuped.txt
touch -d "1 hours ago" stage.00004/file_symlink_previously_backuped.txt

# Backupscript should copy this file
echo "This is a symlink file at the source, and did not exists in a previous backup" > Ext/file_symlink_notyet_backuped.txt
ln -s ${SCRIPTPATH}/result/Ext/file_symlink_notyet_backuped.txt     In/file_symlink_notyet_backuped.txt

# Backupscript largefile; with slightly different content halfway
dd if=/dev/zero of=stage.00004/file_large.txt bs=1000M  count=1

dd if=/dev/zero of=Ext/file_large.txt bs=1000M  count=1
ln -s ${SCRIPTPATH}/result/Ext/file_large.txt                       In/file_large.txt
printf '\x31\xc0\xc3' | dd of=stage.00004/file_large.txt  bs=1 seek=500000000 count=3 conv=notrunc
diff Ext/file_large.txt stage.00004/file_large.txt

# Create the SWIRRL META files
cat > stage.00001/${STAGE_META_FILENAME} << EOF
{
    "stageNumber": "00001",
    "stageCreateTime": "1990-01-01 11:11:11",
    "files": [
        {
          "filename": "file_persistent_in_backup.txt",
          "id": "stage1_id1",
          "state": "new",
          "sourceUrl": "http://fake.nu/persistent"
        },
        {
          "filename": "file_symlink_previously_backuped.txt",
          "id": "stage1_id2",
          "state": "new",
          "sourceUrl": "http://fake.nu/previously"
        }
    ]
}
EOF

cat > stage.00004/${STAGE_META_FILENAME} << EOF
{
    "stageNumber": "00004",
    "stageCreateTime": "1991-02-02 12:12:12",
    "files": [
        {
          "filename": "file_persistent_in_backup.txt",
          "id": "stage1_id1",
          "state": "unchanged",
          "sourceUrl": "http://fake.nu/persistent"
        },
        {
        "filename": "file_symlink_previously_backuped.txt",
          "id": "stage1_id2",
          "state": "unchanged",
          "sourceUrl": "http://fake.nu/previously"
        },
        {
        "filename": "file_large.txt",
          "id": "stage4_id",
          "state": "new",
          "sourceUrl": "http://fake.nu/large"
        }
    ]
}
EOF

# Create the URL files. One for each file to be downloaded.
echo "http://fake.nu/large_new" > ${SCRIPTPATH}/result/Ext/file_large.txt.__url__
ln -s ${SCRIPTPATH}/result/Ext/file_large.txt.__url__ ${SCRIPTPATH}/result/In/file_large.txt.__url__

echo "http://fake.nu/previously_new" > ${SCRIPTPATH}/result/Ext/file_symlink_previously_backuped.txt.__url__
ln -s ${SCRIPTPATH}/result/Ext/file_symlink_previously_backuped.txt.__url__ ${SCRIPTPATH}/result/In/file_symlink_previously_backuped.txt.__url__

echo "http://fake.nu/notyet_new" > ${SCRIPTPATH}/result/Ext/file_symlink_notyet_backuped.txt.__url__
ln -s ${SCRIPTPATH}/result/Ext/file_symlink_notyet_backuped.txt.__url__ ${SCRIPTPATH}/result/In/file_symlink_notyet_backuped.txt.__url__


echo
echo "*** Before:"
ls -lia *

echo
echo
echo "*** Backing up:"
cd ..
time ./backup.sh ${SCRIPTPATH}/result/ ${SCRIPTPATH}/result/In/file_symlink_previously_backuped.txt ${SCRIPTPATH}/result/In/file_symlink_notyet_backuped.txt ${SCRIPTPATH}/result/In/file_large.txt \
                  ${SCRIPTPATH}/result/In/file_symlink_previously_backuped.txt.__url__ ${SCRIPTPATH}/result/In/file_symlink_notyet_backuped.txt.__url__ ${SCRIPTPATH}/result/In/file_large.txt.__url__
cd result


echo
echo
echo "*** After:"
ls -lia *


echo "*** check linking"
inodea=`stat -c '%i' ${SCRIPTPATH}/result/stage.00004/file_symlink_previously_backuped.txt`
inodeb=`stat -c '%i' ${SCRIPTPATH}/result/stage.00005/file_symlink_previously_backuped.txt 2> /dev/null`

inodec=`stat -c '%i' ${SCRIPTPATH}/result/stage.00005/file_symlink_notyet_backuped.txt 2> /dev/null`

inoded=`stat -c '%i' ${SCRIPTPATH}/result/stage.00004/file_large.txt`
inodee=`stat -c '%i' ${SCRIPTPATH}/result/stage.00005/file_large.txt 2> /dev/null`

inodef=`stat -c '%i' ${SCRIPTPATH}/result/stage.00004/file_persistent_in_backup.txt`
inodeg=`stat -c '%i' ${SCRIPTPATH}/result/stage.00005/file_persistent_in_backup.txt 2> /dev/null`

echo
echo
assert_equal "${inodea}" "${inodeb}" "Hardlink creation failed for file_symlink_previously_backuped.txt"
assert_not_empty "${inodec}" "Copy failed for file_symlink_notyet_backuped.txt"
assert_not_equal "${inoded}" "${inodee}" "File comparison or creation failed for file_large.txt"
assert_not_empty "${inodee}" "File comparison or creation failed for file_large.txt"
assert_equal "${inodef}" "${inodeg}" "Hardlink was lost for earlier backuped file folder/file_persistent_in_backup.txt"
assert_not_empty "${inodeg}" "Hardlink was lost for earlier backuped file folder/file_persistent_in_backup.txt"


echo "*** Check file info in ${created_meta}"
if [ ! -f "${created_meta}" ] ; then
  echo "*** Error: Test Failed! ${STAGE_META_FILENAME} is not created"
  cleanup
  exit 1
fi

get_meta_file_info file_symlink_previously_backuped.txt
assert_equal "${created_id}" "${previous_id}" "ID of file file_symlink_previously_backuped.txt is changed"
assert_equal "${created_state}" "unchanged"  "State of file file_symlink_previously_backuped.txt is not unchanged"
assert_equal "${created_source}" "http://fake.nu/previously" "Incorrect URL for file file_symlink_previously_backuped.txt"
assert_equal "${created_previd}" "null" "A prevId is present for file file_symlink_previously_backuped.txt, though file is not changed "

get_meta_file_info file_symlink_notyet_backuped.txt
assert_not_empty "${created_id}" "ID of file file_symlink_notyet_backuped.txt is not generated"
assert_equal "${created_state}" "new"  "State of file file_symlink_notyet_backuped.txt is not new"
assert_equal "${created_source}" "http://fake.nu/notyet_new" "Incorrect URL for file file_symlink_notyet_backuped.txt"
assert_equal "${created_previd}" "null" "A prevId is present for file file_symlink_notyet_backuped.txt, though file is not changed "

get_meta_file_info file_large.txt
assert_not_equal "${created_id}" "${previous_id}" "ID of file file_large.txt is not generated"
assert_not_empty "${created_id}" "ID of file file file_large.txt is not generated"
assert_equal "${created_state}" "updated"  "State of file file_large.txt is not updated"
assert_equal "${created_source}" "http://fake.nu/large_new" "Incorrect URL for file file_large.txt"
assert_equal "${created_previd}" "${previous_id}" "A prevId for file file_symlink_notyet_backuped.txt is not correct"

get_meta_file_info file_persistent_in_backup.txt
assert_equal "${created_id}" "${previous_id}" "ID of file file_persistent_in_backup.txt is changed"
assert_equal "${created_state}" "unchanged"  "State of file file_persistent_in_backup.txt is not unchanged"
assert_equal "${created_source}" "http://fake.nu/persistent" "Incorrect URL for file file_persistent_in_backup.txt"
assert_equal "${created_previd}" "null" "A prevId is present for file file_persistent_in_backup.txt, though file is not changed"


assert_not_equal "${created_stagenum}" "${previous_stagenum}" "No new stage number in ${STAGE_META_FILENAME}"
assert_not_empty "${created_stagenum}" "Empty stage number in ${STAGE_META_FILENAME}"
assert_equal "~/data/staginghistory/stage.${created_stagenum}/" "${created_stagepath}" "Incorrect stage path in ${STAGE_META_FILENAME}"

cleanup
echo "*** Tests succeeded"

exit 0
