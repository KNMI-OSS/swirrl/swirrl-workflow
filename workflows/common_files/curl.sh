#!/bin/bash

function usage(){
  echo " Error"
  echo " Usage: ${0} <download URL> <local filename> <URL filename>"
}

set -e

if [ $# -lt 3 ] 
then
  usage
  exit 1
fi

echo "$EXPRESS_ID"
echo "${TOKEN_ENDPOINT}${EXPRESS_ID}"

if [ -z "$EXPRESS_ID" ]
then
  echo "Using curl to download : ${1} to ${2}"
  curl "${1}" -S -f -o "${2}"
else
  echo "Retrieving access token"
  export TOKEN=$(curl "${TOKEN_ENDPOINT}${EXPRESS_ID}?key=${EXPRESS_KEY}")
  echo "Using curl to download : ${1} to ${2}"
  curl -H "Authorization: Bearer $TOKEN" "${1}" -S -f -o "${2}"
fi

echo "Creating file ${2}.url containing the URL: ${1}"
echo "${1}" > ${3}
