#!/bin/bash

if [ -z "${WF_OPENDAP_IMAGE_TAG}" ] ; then WF_OPENDAP_IMAGE_TAG=`git branch --no-color | grep '*' | cut -d ' ' -f 2` ; fi

docker build -t swirrl-api/workflow/opendap:${WF_OPENDAP_IMAGE_TAG} -f opendap/Dockerfile . $@ || exit 1

if [ ! -z "${CI_REGISTRY_IMAGE}" ] ; then
  docker tag swirrl-api/workflow/opendap:${WF_OPENDAP_IMAGE_TAG} \
    ${CI_REGISTRY_IMAGE}/workflow/opendap:${WF_OPENDAP_IMAGE_TAG}
  docker push ${CI_REGISTRY_IMAGE}/workflow/opendap:${WF_OPENDAP_IMAGE_TAG}
fi
