#!/bin/bash

# Script for running workflow standalone. Should never be needed in production
# as this is done from the swirrl-api, but can be useful for testing and
# debugging. The script uses the git tag to substitute WF_OPENDAP_IMAGE_TAG
# in the k8s deployment spec unless a different value is given with the -t
# option or the environment variable WF_OPENDAP_IMAGE_TAG. You can also set
# CI_REGISTRY_IMAGE environment variable if the image has been pushed to the
# registry. You can specify which of the examples in the example folder is ran
# by specifying it with the -f option.

while getopts u:s:t:f: option ; do
    case ${option} in
        (u) export USERID=${OPTARG}
            ;;
        (s) export SESSIONID=${OPTARG}
            ;;
        (t) export WF_OPENDAP_IMAGE_TAG=${OPTARG}
            ;;
        (f) export INPUTS=${OPTARG}
    esac
done

if [ -z "${INPUTS}" ]; then export INPUTS="inputs-time.yml"; fi
if [ -z "${USERID}" ]; then export USERID=${USER}; fi
if [ -z "${SESSIONID}" ]; then export SESSIONID=`uuidgen`; fi
if [ -z "${WF_OPENDAP_IMAGE_TAG}" ]; then export WF_OPENDAP_IMAGE_TAG=`git branch --no-color | grep '*' | cut -d ' ' -f 2` ; fi
if [ -z "${CI_REGISTRY_IMAGE}" ]; then export CI_REGISTRY_IMAGE=swirrl-api ; fi

kubectl create -n swirrl configmap --dry-run -o yaml \
  --from-file=inputs.yml=example/${INPUTS} \
  cwl-opendap-configmap-inputs-${USERID}-${SESSIONID} > configmap-inputs-${USERID}-${SESSIONID}.yaml

kubectl create -n swirrl configmap --dry-run -o yaml \
  --from-file=../../src/main/resources/k8s-specs/workflow/opendap/job.cwl \
  --from-file=../../src/main/resources/k8s-specs/workflow/opendap/subset_and_download.cwl \
  --from-file=../../src/main/resources/k8s-specs/workflow/opendap/urllist_to_file.cwl \
  cwl-opendap-configmap-workflows-${USERID}-${SESSIONID} > configmap-workflows-${USERID}-${SESSIONID}.yaml

cat ../../src/main/resources/k8s-specs/volume/pvc.yaml | envsubst > pvc-data-${USERID}-${SESSIONID}.yaml
cat ../../src/main/resources/k8s-specs/workflow/opendap/job.yaml | envsubst > job-${USERID}-${SESSIONID}.yaml

kubectl create -n swirrl \
  -f configmap-workflows-${USERID}-${SESSIONID}.yaml \
  -f configmap-inputs-${USERID}-${SESSIONID}.yaml \
  -f pvc-data-${USERID}-${SESSIONID}.yaml \
  -f job-${USERID}-${SESSIONID}.yaml

wfpod=$(kubectl get -n swirrl pod | grep wf-opendap-${USERID}-${SESSIONID} | cut -d ' ' -f 1)
echo
echo "Started job with id ${SESSIONID}"
echo "Check logs with:"
echo "kubectl logs -n swirrl ${wfpod}"
echo

envsubst < ../../src/main/resources/k8s-specs/notebook/pvc.yaml > pvc-work-${USERID}-${SESSIONID}.yaml
envsubst < ../../src/main/resources/k8s-specs/service/deployment.yaml > deployment-${USERID}-${SESSIONID}.yaml

nbpod="kubectl get -n swirrl pod | grep jupyter-${USERID}-${SESSIONID} | cut -d ' ' -f 1"

cat <<EOF

======================================================================
To create a notebook without ingress or service to inspect the result:

kubectl create -n swirrl -f pvc-work-${USERID}-${SESSIONID}.yaml
kubectl create -n swirrl -f deployment-${USERID}-${SESSIONID}.yaml

Ignore any errors having to do with readiness. They are caused by the
fact we didn't create a service and ingress for the deployment.

To inspect the result:
kubectl exec -n swirrl \`${nbpod}\` -- ls -lR /home/jovyan/data/
or login to the container:
kubectl exec -n swirrl -ti \`${nbpod}\` /bin/bash
======================================================================

EOF

echo "Cleanup everything with:"
echo ./cleanup.sh -u ${USERID} -s ${SESSIONID}
echo
