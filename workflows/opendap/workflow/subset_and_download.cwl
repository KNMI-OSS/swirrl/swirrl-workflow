#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

# author eu-team@knmi.nl
# cwl descriptions
label: "Extract NetCDF WF"
doc: "Downloads data through OpenDAP."

requirements:
  - class: InlineJavascriptRequirement

baseCommand: ['python3', '-u', '/usr/local/bin/subsetnc.py']

inputs:
  urllist:
    type: File
    inputBinding:
      position: 1
  minlat:
    type: string
    inputBinding:
      position: 2
  maxlat:
    type: string
    inputBinding:
      position: 3
  minlon:
    type: string
    inputBinding:
      position: 4
  maxlon:
    type: string
    inputBinding:
      position: 5
  start_time:
    type: string?
    inputBinding:
      position: 6
  stop_time:
    type: string?
    inputBinding:
      position: 7

# files downloaded in parallel expected as cwl outputs
outputs:
  outfiles:
    type:
      type: array
      items: File
    outputBinding:
      glob: 'results/*.nc'
