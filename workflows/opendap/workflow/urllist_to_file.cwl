#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: "Convert file list to a file."

inputs:
  links:
    type:
      type: array
      items: string
    inputBinding:
      position: 1
  urllist_filename:
    type: string
outputs:
  urllist:
    type: stdout
stdout: $(inputs.urllist_filename)
baseCommand: ['echo', '-e']
