#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: Workflow

# author eu-team@knmi.nl
# cwl descriptions
label: "OpenDAP download workflow"
doc: "Downloads data based on a list of URLs and combine them based on spatial and temporal parameters."

# patern description in
# http://www.commonwl.org/v1.0/UserGuide.html#Writing_Workflows

# cwl requirements necessary for workflows and scatter to work.
# used to allow software diversity within the workflow.
requirements:
  - class: SubworkflowFeatureRequirement
  - class: InlineJavascriptRequirement

# array of urls passed to workflow as cwl inputs
inputs:
  links:
    type:
      type: array
      items: string
  urllist_filename:
    type: string
  minlat:
    type: string
  maxlat:
    type: string
  minlon:
    type: string
  maxlon:
    type: string
  start_time:
    type: string?
  stop_time:
    type: string?

# files downloaded in parallel expected as cwl outputs
outputs:
  outfiles:
    type:
      type: array
      items: File
    outputSource: "#subset_and_download/outfiles"

# workflow steps defined, each step is a parallelizable process when orchestrated under relevant conditions
steps:
  urllist_to_file:
    in:
      links: links
      urllist_filename: urllist_filename
    out:
      [urllist]
    run:
      urllist_to_file.cwl

  subset_and_download:
    in:
      urllist: urllist_to_file/urllist
      minlat: minlat
      maxlat: maxlat
      minlon: minlon
      maxlon: maxlon
      start_time: start_time
      stop_time: stop_time
    out:
      [outfiles]
    run:
      subset_and_download.cwl

  backup:
    run:
      class: CommandLineTool
      baseCommand: [ 'backup.sh' ]
      arguments: [ '/data/outputs/staginghistory/', $(inputs.job), $(inputs.src) ]
      inputs:
        job:
          type:
            items: File
            type: array
        src:
          type: File

      outputs: [ ]
    in:
      job: [ subset_and_download/outfiles ]
      src: [ urllist_to_file/urllist ]
    out: [ ]
