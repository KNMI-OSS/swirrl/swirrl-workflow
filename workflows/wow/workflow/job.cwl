#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: Workflow

# author eu-team@knmi.nl
# cwl descriptions
label: "WOW-NL extracting workflow"
doc: "Downloads subsetted data from a specified dataset using spatial and temporal parameters."

# patern description in
# http://www.commonwl.org/v1.0/UserGuide.html#Writing_Workflows

# cwl requirements necessary for workflows and scatter to work.
# used to allow software diversity within the workflow.
requirements:
  - class: SubworkflowFeatureRequirement
  - class: InlineJavascriptRequirement

# array of urls passed to workflow as cwl inputs
inputs:
  outfiletype:
    type: string
  bbox_ll_lon:
    type: string
  bbox_ll_lat:
    type: string
  bbox_ur_lon:
    type: string
  bbox_ur_lat:
    type: string
  starttime:
    type: string
  endtime:
    type: string

# files downloaded in parallel expected as cwl outputs
outputs:
  outfiles:
    type:
      type: array
      items: File
    outputSource: "#subset_and_download/download"

# workflow steps defined, each step is a parallelizable process when orchestrated under relevant conditions
steps:
  subset_and_download:
    in:
      outfiletype: outfiletype
      bbox_ll_lon: bbox_ll_lon
      bbox_ll_lat: bbox_ll_lat
      bbox_ur_lon: bbox_ur_lon
      bbox_ur_lat: bbox_ur_lat
      starttime: starttime
      endtime: endtime

    out:
      [download,downloadurl]
    run:
      subset_and_download.cwl

  backup:
    run:
      class: CommandLineTool
      baseCommand: [ 'backup.sh' ]
      arguments: [ '/data/outputs/staginghistory/', $(inputs.job), $(inputs.src) ]
      inputs:
        job:
          type:
            items: File
            type: array
        src:
          type:
            items: File
            type: array

      outputs: [ ]
    in:
      job: [ subset_and_download/download ]
      src: [ subset_and_download/downloadurl ]
    out: [ ]
