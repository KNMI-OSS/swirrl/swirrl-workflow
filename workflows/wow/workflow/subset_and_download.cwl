#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

# author eu-team@knmi.nl
# cwl descriptions
label: "Create WOW-NL netcdf"
doc: "Creates a netcdf file from WOW-NL database."

requirements:
  - class: InlineJavascriptRequirement

baseCommand: ['wow.bash']

inputs:
  outfiletype:
    type: string
    inputBinding:
      position: 1
  bbox_ll_lon:
    type: string
    inputBinding:
      position: 2
  bbox_ll_lat:
    type: string
    inputBinding:
      position: 3
  bbox_ur_lon:
    type: string
    inputBinding:
      position: 4
  bbox_ur_lat:
    type: string
    inputBinding:
      position: 5
  starttime:
    type: string
    inputBinding:
      position: 6
  endtime:
    type: string
    inputBinding:
      position: 7

# files downloaded in parallel expected as cwl outputs
outputs:
  download:
    type:
      type: array
      items: File
    outputBinding:
      glob: |
        ${
          var ext=inputs.outfiletype;
          if (ext === "net-cdf") return "*.nc";
          else if (ext == "csv" ) return "*.csv";
          else return "*.dat";
        }
  downloadurl:
    type:
      type: array
      items: File
    outputBinding:
      glob: 'results/*.__url__'
