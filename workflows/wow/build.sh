#!/bin/bash

if [ -z "${WF_WOW_IMAGE_TAG}" ] ; then WF_WOW_IMAGE_TAG=`git branch --no-color | grep '*' | cut -d ' ' -f 2` ; fi


docker build -t swirrl-workflow/workflow/wow:${WF_WOW_IMAGE_TAG} -f wow/Dockerfile . $@ || exit 1

if [ ! -z "${CI_REGISTRY_IMAGE}" ] ; then
  docker tag swirrl-workflow/workflow/wow:${WF_WOW_IMAGE_TAG} \
    ${CI_REGISTRY_IMAGE}/workflow/wow:${WF_WOW_IMAGE_TAG}
  docker push ${CI_REGISTRY_IMAGE}/workflow/wow:${WF_WOW_IMAGE_TAG}
fi
