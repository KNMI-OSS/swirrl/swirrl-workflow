#!/bin/bash

sleep 10
outfiletype=$1

# Lower left corner
bbox_ll_lon=$2
bbox_ll_lat=$3
# Upper right corner
bbox_ur_lon=$4
bbox_ur_lat=$5

timestamp_begin=$6
timestamp_end=$7

host="wow-prd-api.data.knmi.cloud"
path="researcher/download-${outfiletype}"

case ${outfiletype} in
  "net-cdf")
    outfileext="nc"
    ;;
  "csv")
    outfileext="csv"
    ;;
  *)
    outfileext="dat"
    ;;
esac

echo "Downloading data with parameters:"
echo "agg=sixtyMin"
params="agg=sixtyMin"
echo "bbox=${bbox_ll_lon}"
params="${params}&bbox=${bbox_ll_lon}"
echo "bbox=${bbox_ll_lat}"
params="${params}&bbox=${bbox_ll_lat}"
echo "bbox=${bbox_ur_lon}"
params="${params}&bbox=${bbox_ur_lon}"
echo "bbox=${bbox_ur_lat}"
params="${params}&bbox=${bbox_ur_lat}"
echo "datetime=${timestamp_begin}/${timestamp_end}"
params="${params}&datetime=${datetime}"

outfile=WoW_$(echo ${timestamp_begin} | tr -d ':')_$(echo ${timestamp_end} | tr -d ':').${outfileext}

curl --location --get \
  --data "agg=sixtyMin" \
  --data "bbox=${bbox_ll_lon}" \
  --data "bbox=${bbox_ll_lat}" \
  --data "bbox=${bbox_ur_lon}" \
  --data "bbox=${bbox_ur_lat}" \
  --data-urlencode "datetime=${timestamp_begin}/${timestamp_end}" \
  https://${host}/${path} \
  -H 'accept: application/json' -H 'accept-encoding: br,gzip' \
  --output ${outfile}.br

if [ -e ${outfile}.br ] ; then
  ls -la
  echo "Decompressing downloaded ${outfile}.br from https://${host}/${path}?${params}"
  brotli --decompress ${outfile}.br
  ls -la
  rm -f ${outfile}.br
  mkdir results
  cp ${outfile} results/
else
  echo "Failed to download ${outfile}.br from https://${host}/${path}?${params}"
  exit 1
fi

if [ ! -e ${outfile} ] ; then
  echo "Decompression of downloaded ${outfile}.br failed."
  exit 1
fi
echo "https://${host}/${path}?${params}" > ${outfile}.__url__
cp ${outfile}.__url__ results/
ls -la
echo "Done"

exit 0