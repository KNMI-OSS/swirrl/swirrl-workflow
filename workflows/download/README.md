# Docker image which runs CWL to download data

# Build the image

```
./downlaod/build.sh ## From workflows directory below repository root.
```
or
```
skaffold build -b download ## From the repository root.
```

Using the `build.sh` script, the image's tag will be set to the git branch name.
You can override it by setting the WF_OPENDAP_IMAGE_TAG environment variable.
If CI_REGISTRY_IMAGE has been set, which is the case in gitlab CI/CD, then the
image will also be pushed to the registry.

Set this by hand if needed with:
```shell script
export WF_DOWNLOAD_IMAGE_TAG=`git branch --no-color | grep '*' | cut -d ' ' -f 2`
```

# Run a container

Run the workflow with default inputs ("accept_url_array_scatter.cwl", "inputs.yml"),
replace `swirrl-api` with the gitlab registry if you want to use a pushed image.
```
docker run swirrl-api/workflow/download:${WF_DOWNLOAD_IMAGE_TAG}
```

Run the workflow with specified inputs:
```
mkdir output
docker run \
    -v example:/data/inputs \
    -v output:/data/outputs \
    swirrl-api/workflow/download:${WF_DOWNLOAD_IMAGE_TAG} <cwl_input_file>
```

`<cwl_input_file>` needs to be a yaml file of the same form as
inputs.yml in the example directory. In the example above it will be found
in the example directory below the current directory on the host, i.e. 
/data/inputs inside the container. Don't specify a path above this directory.
It is unlikely that you will see anything in `./output` after the run
has completed, because the container has no permission to write to the host's
file system. In order to fix this run the command below to map the user id. 

For development, mapping pretty much everything and setting the user id
to your user ID to ensure write permissions for the docker container:
```
docker run -u `id -u`  \
    -v `pwd`/../../src/main/resources/k8s-specs/workflow/download:/data/workflows \
    -v `pwd`/example:/data/inputs \
    -v `pwd`/output:/data/outputs \
    -v `pwd`/bin/backup.sh:/usr/local/bin/backup.sh \
    -v `pwd`/bin/backup.py:/usr/local/bin/backup.py \
    -v `pwd`/bin/curl.sh:/usr/local/bin/curl.sh \
    swirrl-api/workflow/download:${WF_DOWNLOAD_IMAGE_TAG} <cwl_input_file>
```
Editing a workflow or the bin/backup.sh script now will not require a
rebuild of the docker image. Just run it again with the above command.

### Backups of downloaded data

The downloaded data is backed up, so we can refer to earlier versions
later.  This is done by copying all output files to
/data/staginghistory/outputs (we can't use /data/outputs, because the data
will not be there until the cwl-runner has performed *all* workflow
steps, including the backup step). After each file is copied, earlier
versions of download actions are stored in
  /data/staginghistory/stage.00001 (initial backup),
  /data/staginghistory/stage.00002 (newer backup),
  /data/staginghistory/stage.00003 (newest backup),  etc
This is done through clever hard linking so it will not take up extra data
unless a file is changed.

## Running the mockup script:

Run `./run.sh` with optional environment variables:

* `USERID` (default=$USER from user's environment)
* `SESSIONID` (default=output from uuidgen)
* `WF_DOWNLOAD_IMAGE_TAG` (default=git branch name)
* `CI_REGISTRY_IMAGE` (default=empty)

This will output `configmap-inputs-${USERID}-${SESSIONID}.yaml`,
`configmap-workflows-${USERID}-${SESSIONID}.yaml`
`job-subst-${USERID}-${SESSIONID}.yaml` and `pvc-subst-${USERID}-${SESSIONID}.yaml`.

These are then deployed with the following command:
```
kubectl create -n swirrl \
    -f configmap-inputs-${USERID}-${SESSIONID}.yaml \ 
    -f configmap-workflows-${USERID}-${SESSIONID}.yaml \
    -f job-${USERID}-${SESSIONID}.yaml \
    -f pvc-${USERID}-${SESSIONID}.yaml
```

NB Please note that removing and recreating a PVC that's currently
mounted by a running container will leave the volume attached to that
container and create a new one for the job, meaning they won't share a
volume at that point.
