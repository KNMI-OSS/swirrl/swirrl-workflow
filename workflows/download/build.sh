#!/bin/bash

if [ -z "${WF_DOWNLOAD_IMAGE_TAG}" ] ; then WF_DOWNLOAD_IMAGE_TAG=`git branch --no-color | grep '*' | cut -d ' ' -f 2` ; fi

docker build -t swirrl-api/workflow/download:${WF_DOWNLOAD_IMAGE_TAG} -f download/Dockerfile . $@ || exit 1

if [ ! -z "${CI_REGISTRY_IMAGE}" ] ; then
  echo "Tagging ${CI_REGISTRY_IMAGE}/workflow/download:${WF_DOWNLOAD_IMAGE_TAG}"
  docker tag swirrl-api/workflow/download:${WF_DOWNLOAD_IMAGE_TAG} \
    ${CI_REGISTRY_IMAGE}/workflow/download:${WF_DOWNLOAD_IMAGE_TAG}
  docker push ${CI_REGISTRY_IMAGE}/workflow/download:${WF_DOWNLOAD_IMAGE_TAG}
fi
