#!/bin/bash

# Script for running workflow standalone. Should never be needed in production
# as this is done from the swirrl-api, but can be useful for testing and
# debugging. The script uses the git tag to substitute WF_DOWNLOAD_IMAGE_TAG
# in the k8s deployment spec unless a different value is given with the -t
# option or the environment variable WF_DOWNLOAD_IMAGE_TAG. You can also set
# CI_REGISTRY_IMAGE environment variable if the image has been pushed to the
# registry.

while getopts u:s:b: option ; do
    case ${option} in
        (u) export USERID=${OPTARG}
            ;;
        (s) export SESSIONID=${OPTARG}
            ;;
        (t) export WF_DOWNLOAD_IMAGE_TAG=${OPTARG}
    esac
done

if [ -z "${USERID}" ]; then export USERID=${USER}; fi
if [ -z "${SESSIONID}" ]; then echo "Need to supply SESSIONID in env or with -s option"; exit 1; fi
if [ -z "${WF_DOWNLOAD_IMAGE_TAG}" ]; then export WF_DOWNLOAD_IMAGE_TAG=`git branch --no-color | grep '*' | cut -d ' ' -f 2` ; fi
if [ -z "${CI_REGISTRY_IMAGE}" ]; then export ${CI_REGISTRY_IMAGE}=swirrl-api ; fi

kubectl get -n swirrl -f deployment-${USERID}-${SESSIONID}.yaml && \
  kubectl delete -n swirrl -f deployment-${USERID}-${SESSIONID}.yaml
rm -f deployment-${USERID}-${SESSIONID}.yaml

kubectl get -n swirrl -f pvc-work-${USERID}-${SESSIONID}.yaml && \
  kubectl delete -n swirrl -f pvc-work-${USERID}-${SESSIONID}.yaml
rm -f pvc-work-${USERID}-${SESSIONID}.yaml

kubectl get -n swirrl -f configmap-workflows-${USERID}-${SESSIONID}.yaml && \
  kubectl delete -n swirrl -f configmap-workflows-${USERID}-${SESSIONID}.yaml
rm -f configmap-workflows-${USERID}-${SESSIONID}.yaml

kubectl get -n swirrl -f configmap-inputs-${USERID}-${SESSIONID}.yaml && \
  kubectl delete -n swirrl -f configmap-inputs-${USERID}-${SESSIONID}.yaml
rm -f configmap-inputs-${USERID}-${SESSIONID}.yaml

kubectl get -n swirrl -f job-${USERID}-${SESSIONID}.yaml && \
  kubectl delete -n swirrl -f job-${USERID}-${SESSIONID}.yaml
rm -f job-${USERID}-${SESSIONID}.yaml

kubectl get -n swirrl -f pvc-data-${USERID}-${SESSIONID}.yaml && \
  kubectl delete -n swirrl -f pvc-data-${USERID}-${SESSIONID}.yaml
rm -f pvc-data-${USERID}-${SESSIONID}.yaml
