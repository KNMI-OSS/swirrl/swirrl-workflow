#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: Workflow

# author eu-team@knmi.nl
label: "single URL download"
doc: "Curl Workflow: downloads data based on curl input."

# use epos_imputs.yml for demo

inputs:
  message:
    type:
      type: record
      fields:
        url:
          type: string
        filename:
          type: string

outputs:
  download:
    type: File
    outputSource:  "#step0/curl"
  downloadurl:
    type: File
    outputSource: "step0/curl-url"

steps:
  step0:
    run:
      class: CommandLineTool

      # download via curl to fileout location

      baseCommand: [ 'curl.sh' ]
      arguments: [$(inputs.url.url), $(inputs.url.filename), $(inputs.url.filename + '.__url__')]

      inputs:
        url:
          type:
            type: record
            fields:
              url:
                type: string
              filename:
                type: string
#      stdout: $(inputs.url.filename)

      outputs:
        curl:
          type: File
          outputBinding:
              glob: $(inputs.url.filename)
        curl-url:
            type: File
            outputBinding:
              glob: $(inputs.url.filename + '.__url__')
    in:
      url: message

    out: [curl, curl-url]
