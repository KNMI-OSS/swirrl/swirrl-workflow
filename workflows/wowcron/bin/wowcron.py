import os

import datetime
import sys
from urllib import request, parse
import base64
import yaml
import json

inputsfile = sys.argv[1]
sessionId = os.environ['SESSION_ID']
with open("/data/inputs/metadata.txt", "r") as runIdFile:
    runId = runIdFile.read()

with open(inputsfile) as infile:
    inputArgs = yaml.safe_load(infile)

    outfiletype = inputArgs['outfiletype']
    # Lower left corner
    bbox_ll_lon = inputArgs['bbox_ll_lon']
    bbox_ll_lat = inputArgs['bbox_ll_lat']
    # Upper right corner
    bbox_ur_lon = inputArgs['bbox_ur_lon']
    bbox_ur_lat = inputArgs['bbox_ur_lat']

curdate = datetime.datetime.utcnow()
begindate = (curdate - datetime.timedelta(days=1)).replace(hour=0, minute=0, second=0)
enddate = (curdate - datetime.timedelta(days=1)).replace(hour=23, minute=59, second=59)

runOnceInputYaml = """
name: "WOW-NL downloader"
message: "Download WOW-NL data"

outfiletype: "%s"
bbox_ll_lon: "%s"
bbox_ll_lat: "%s"
bbox_ur_lon: "%s"
bbox_ur_lat: "%s"
starttime: "%s"
endtime: "%s"
""" % (outfiletype, bbox_ll_lon, bbox_ll_lat, bbox_ur_lon, bbox_ur_lat,
       begindate.strftime("%Y-%m-%dT%H:%M:%SZ"), enddate.strftime("%Y-%m-%dT%H:%M:%SZ"))

print("Input for run once job of session", sessionId)
print(runOnceInputYaml)

requestPayload = {
    "sessionId": sessionId,
    "inputs": base64.b64encode(bytes(runOnceInputYaml, 'utf-8')).decode('utf-8')
}
print("HTTP request payload to swirrl-api")
print(json.dumps(requestPayload).encode('utf-8'))
req = request.Request(url="http://swirrl-api/swirrl-api/v1.0/workflow/wow/run/",
                      data=json.dumps(requestPayload).encode('utf-8'), method="POST")
req.add_header("Content-Type", "application/json")
req.add_header("Accept", "application/json")
req.add_header("agent", f"wowcron-{runId}")
with request.urlopen(req) as response:
    if response.status == 200:
        body = json.loads(response.read())
        print(body)
    else:
        sys.exit(1)
sys.exit(0)