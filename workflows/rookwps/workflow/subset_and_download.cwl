#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

# author eu-team@knmi.nl
# cwl descriptions
label: "Extract NetCDF WF"
doc: "Downloads data through OpenDAP."

requirements:
  - class: InlineJavascriptRequirement

baseCommand: ['python3', '-u', '/usr/local/bin/subset_rook_wps.py']

inputs:
  datasetid:
    type: string
    inputBinding:
      position: 1
  minlat:
    type: string
    inputBinding:
      position: 2
  maxlat:
    type: string
    inputBinding:
      position: 3
  minlon:
    type: string
    inputBinding:
      position: 4
  maxlon:
    type: string
    inputBinding:
      position: 5
  start_time:
    type: string?
    inputBinding:
      position: 6
  stop_time:
    type: string
    inputBinding:
      position: 7
  parameter:
    type: string
    inputBinding:
      position: 8
  operation:
    type: string
    inputBinding:
      position: 9
  rookurl:
    type: string
    inputBinding:
      position: 10

# files downloaded in parallel expected as cwl outputs
outputs:
  download:
    type:
      type: array
      items: File
    outputBinding:
      glob: 'results/*.nc'
  downloadurl:
    type:
      type: array
      items: File
    outputBinding:
      glob: 'results/*.__url__'
