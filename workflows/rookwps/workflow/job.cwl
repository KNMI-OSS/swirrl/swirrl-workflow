#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: Workflow

# author eu-team@knmi.nl
# cwl descriptions
label: "ROOK WPS subsetting workflow"
doc: "Downloads subsetted data from a specified dataset using spatial and temporal parameters."

# patern description in
# http://www.commonwl.org/v1.0/UserGuide.html#Writing_Workflows

# cwl requirements necessary for workflows and scatter to work.
# used to allow software diversity within the workflow.
requirements:
  - class: SubworkflowFeatureRequirement
  - class: InlineJavascriptRequirement

# array of urls passed to workflow as cwl inputs
inputs:
  datasetid:
    type: string
  minlat:
    type: string
  maxlat:
    type: string
  minlon:
    type: string
  maxlon:
    type: string
  start_time:
    type: string
  stop_time:
    type: string
  parameter:
    type: string
  operation:
    type: string
  rookurl:
    type: string

# files downloaded in parallel expected as cwl outputs
outputs:
  outfiles:
    type:
      type: array
      items: File
    outputSource: "#subset_and_download/download"

# workflow steps defined, each step is a parallelizable process when orchestrated under relevant conditions
steps:
  subset_and_download:
    in:
      datasetid: datasetid
      minlat: minlat
      maxlat: maxlat
      minlon: minlon
      maxlon: maxlon
      start_time: start_time
      stop_time: stop_time
      parameter: parameter
      operation: operation
      rookurl: rookurl

    out:
      [download,downloadurl]
    run:
      subset_and_download.cwl

  backup:
    run:
      class: CommandLineTool
      baseCommand: [ 'backup.sh' ]
      arguments: [ '/data/outputs/staginghistory/', $(inputs.job), $(inputs.src) ]
      inputs:
        job:
          type:
            items: File
            type: array
        src:
          type:
            items: File
            type: array

      outputs: [ ]
    in:
      job: [ subset_and_download/download ]
      src: [ subset_and_download/downloadurl ]
    out: [ ]