import sys, os, os.path
from rooki import operators as ops
from prov.model import ProvDocument
import json
import requests
import re

print(sys.argv)

if not( os.path.isdir('results')):
    os.mkdir('results')

dataset_id = sys.argv[1]

minlat = sys.argv[2]
maxlat = sys.argv[3]
minlon = sys.argv[4]
maxlon = sys.argv[5]

starttime = sys.argv[6]
stoptime = sys.argv[7]

parameter = sys.argv[8]
operation = sys.argv[9].lower()
rookurl = sys.argv[10]

os.environ['ROOK_URL'] = rookurl
os.environ['ROOK_MODE'] = 'sync'

api_key_dict = {}

if 'cmcc.it' in rookurl:
    api_key_dict = {'api_key': os.environ['API_KEY']}

bounding_box = ','.join((minlon, maxlat, maxlon, minlat))
temporal_range = '/'.join((starttime+'T00:00', stoptime+'T00:00'))

subset = ops.Subset(
    ops.Input(
        parameter, [dataset_id]
    ),
    time=temporal_range,
    area=bounding_box,
    **api_key_dict,
)
operations = {"average":ops.Average}
resp = None
if operation in operations:
    print("Calling operation %s" % operation)
    wf = operations[operation](subset, dims="time", **api_key_dict)
    resp = wf.orchestrate()
else:
    print("Not calling any operation get the entire subset.")
    resp = subset.orchestrate()

if not resp.ok:
    print("Response from ROOK not ok")
    sys.exit(1)

print("ROOK nr of downloaded files: %d" % resp.num_files)
print("ROOK download URLs: %s" % resp.download_urls())
resp.output_dir = "results"
files = resp.download()

for url in resp.download_urls():
    with open("results/%s.__url__" % os.path.basename(url), "w") as urlfile:
        urlfile.write(url)

print("Computing ROOK provenance")
json_prov = requests.get(resp.provenance()).json()
json_prov['prefix']['uuid'] = "urn:uuid:"
json_prov['prefix']['owl'] = "http://www.w3.org/2002/07/owl#"
for activity in json_prov['activity']:
    json_prov['activity'][activity]['provone:wasPartOf'] = { "$": "uuid:<run_id>", "type": "prov:QUALIFIED_NAME" }

for key, entity in json_prov['entity'].items():
    if entity['prov:type']['$'] == 'provone:Data':
        is_generated = any(x['prov:generatedEntity'] == key for x in json_prov['wasDerivedFrom'].values())
        is_used = any(x['prov:usedEntity'] == key for x in json_prov['wasDerivedFrom'].values())
        if not is_used and is_generated:
            entity['owl:sameAs'] = { "$": f"uuid:<{entity['prov:label']}>", "type": "prov:QUALIFIED_NAME" }

provdoc = ProvDocument()
prov_object = provdoc.deserialize(content=json.dumps(json_prov), format="json")
provn_prov = prov_object.serialize(format="provn")

print(provn_prov)

if not files:
    print("Nothing downloaded.")
    sys.exit(1)

sys.exit(0)
