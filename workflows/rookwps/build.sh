#!/bin/bash

if [ -z "${WF_ROOKWPS_IMAGE_TAG}" ] ; then WF_ROOKWPS_IMAGE_TAG=`git branch --no-color | grep '*' | cut -d ' ' -f 2` ; fi

docker build -t swirrl-workflow/workflow/rookwps:${WF_ROOKWPS_IMAGE_TAG} -f rookwps/Dockerfile . $@ || exit 1

if [ ! -z "${CI_REGISTRY_IMAGE}" ] ; then
  docker tag swirrl-workflow/workflow/rookwps:${WF_ROOKWPS_IMAGE_TAG} \
    ${CI_REGISTRY_IMAGE}/workflow/rookwps:${WF_ROOKWPS_IMAGE_TAG}
  docker push ${CI_REGISTRY_IMAGE}/workflow/rookwps:${WF_ROOKWPS_IMAGE_TAG}
fi
