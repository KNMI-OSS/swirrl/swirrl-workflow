#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: Workflow

# author eu-team@knmi.nl
# cwl descriptions
doc: "Downloads data based on an S3 bucket name, dataset name and version from KNMI's data platform."

# patern description in
# http://www.commonwl.org/v1.0/UserGuide.html#Writing_Workflows

# cwl requirements necessary for workflows and scatter to work.
# used to allow software diversity within the workflow.
requirements:
  - class: SubworkflowFeatureRequirement
  - class: InlineJavascriptRequirement

# array of urls passed to workflow as cwl inputs
inputs:
  s3bucket:
    type: string
  dataset_name:
    type: string
  dataset_version:
    type: string

# files downloaded in parallel expected as cwl outputs
outputs:
  download:
    type:
      type: array
      items: File
    outputSource: "#aws_s3_sync/download"
  s3location:
    type:
      type: array
      items: File
    outputSource: "#aws_s3_sync/s3location"

# workflow steps defined, each step is a parallelizable process when orchestrated under relevant conditions
steps:
  aws_s3_sync:
    in:
      s3bucket: s3bucket
      dataset_name: dataset_name
      dataset_version: dataset_version
    out:
      [download,s3location]
    run:
      aws_s3_sync.cwl

  backup:
    run:
      class: CommandLineTool
      baseCommand: [ 'backup.sh' ]
      arguments: [ '/data/outputs/staginghistory/', $(inputs.job), $(inputs.src) ]
      inputs:
        job:
          type:
            items: File
            type: array
        src:
          type:
            items: File
            type: array

      outputs: [ ]
    in:
      job: [ aws_s3_sync/download ]
      src: [ aws_s3_sync/s3location ]
    out: [ ]
label: "KNMI Dataplatform download workflow"
