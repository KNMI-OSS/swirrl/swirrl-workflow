#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

# author eu-team@knmi.nl
# cwl descriptions
label: "AWS S3 sync"
doc: "Syncs the data inside an S3 prefix"

# patern description in
# http://www.commonwl.org/v1.0/UserGuide.html#Writing_Workflows

# cwl requirements necessary for workflows and scatter to work.
# used to allow software diversity within the workflow.
requirements:
  - class: InlineJavascriptRequirement

baseCommand: [ '/usr/local/bin/s3_sync.sh' ]

inputs:
  s3bucket:
    type: string
    inputBinding:
      position: 1
  dataset_name:
    type: string
    inputBinding:
      position: 2
  dataset_version:
    type: string
    inputBinding:
      position: 3

outputs:
  download:
    type:
      type: array
      items: File
    outputBinding:
      glob: "results/*"
  s3location:
    type:
      type: array
      items: File
    outputBinding:
      glob: "urls/*"