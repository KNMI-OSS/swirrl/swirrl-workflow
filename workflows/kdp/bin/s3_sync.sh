#!/bin/bash -x

mkdir -p $(pwd)/results $(pwd)/urls
aws s3 sync "s3://${1}/${2}/${3}"  ./results

for x in ./results/*; do
  filename="$(basename $x)"
  echo "s3://${1}/${2}/${3}/" > "$(pwd)/urls/${filename}.__url__"
done


#for local testing:
#for i in `seq 5` ; do
#  cat /etc/passwd > $(pwd)/results/mockfile_${1}_${2}_${3}_${i}
#  echo "s3://${1}/${2}/${3}/mockfile_${1}_${2}_${3}_${i}" > $(pwd)/urls/mockfile_${1}_${2}_${3}_${i}.__url__
#done
