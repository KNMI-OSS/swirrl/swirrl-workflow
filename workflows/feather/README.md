# Workflow to download and create feather files

## Design

This workflow will parse the provided input json, which is a `dataset_specs.json`
file as indicated by NORCE. It will create an `input.yml` file that is then sent to
the `workflow/download/run` end point to download the feather file indicated in the
URL field of `dataset_specs.json`.

Then it will call the `workflow/feather/run` end point providing both the `dataset_specs.json`
and the downloaded `earthquakes.txt` file to create a feather file.

This would mean that there will be three provenance entries for this job. One for
the download job, two for the feather job.