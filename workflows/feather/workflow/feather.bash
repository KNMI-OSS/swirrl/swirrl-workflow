#!/bin/bash

echo "Starting feather processing job"
ls -l /data/inputs
sleep 20
runid=$(cat /data/inputs/metadata.txt)
echo "Run Id: ${runid}"

mkdir -p /app/input
mkdir -p /app/output
cp /data/inputs/inputs.yml /app/input/dataset_specs.json

# Figure out the URL with jq.
# Select the url from the object that has an 'input_file' key
declare -a configmap_opts
declare -a expected_outputfiles
n_inputs=$(jq -r '[.input[] | select(.input_file)] | length' /app/input/dataset_specs.json)
for idx in $(seq ${n_inputs}) ; do
  eval $(jq -r "[.input[] | select(.input_file)][${idx}-1]" /app/input/dataset_specs.json | \
    jq -r '. | to_entries | map("\(.key)=\(.value | @sh)")[]')
  echo "Downloading from ${url}"
  if curl --output /app/input/${input_file} "${url}" ; then
    echo ${url} > /app/output/${input_file}.__url__
    echo ${url} > /app/output/${output_file}.__url__
    expected_outputfiles+=(${output_file})
    expected_outputfiles+=(${input_file})
    configmap_opts+=(--from-file=${input_file}=/app/input/${input_file})
  else
    echo "Could not download input file from ${url}"
    exit 1
  fi
done

# Determine wms_output_file and figure out the WMS URls for it.
wms_output_file=$(jq -r .wms_output_file /app/input/dataset_specs.json)
echo "WMS Output file: ${wms_output_file}"
expected_outputfiles+=(${wms_output_file})
wms_urls=$(jq -r '.input[] | select(.type=="WMS" or .type=="wms").url' /app/input/dataset_specs.json | tr '\n' ',')
echo "WMS URLs: ${wms_urls}"
echo ${wms_urls} > /app/output/${wms_output_file}.__url__

echo "Creating configmap feather-input-${runid}"
kubectl -n swirrl create configmap \
 --from-file=dataset_specs.json=/app/input/dataset_specs.json \
 ${configmap_opts[*]} \
 feather-input-${runid}

echo "Creating persistent volume claim feather-output-${runid}"
kubectl -n swirrl create -f - <<EOPVC
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: feather-output-${runid}
  labels:
    job: feather-processing-${runid}
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 100Mi
EOPVC

echo "Start feather service."
# Run the feather job
kubectl -n swirrl run --attach --quiet --restart=Never --image=norceresearch1/feather-service-epos-sra:2024.01 \
  --pod-running-timeout=30m \
  --overrides="
    {
      \"kind\":\"Pod\",\"apiVersion\":\"v1\",\"metadata\":{\"name\":\"feather-processing-${runid}\"},
      \"spec\":{
        \"serviceAccount\":\"swirrl-wf-service-account\",
        \"imagePullSecrets\":[{\"name\":\"feather-deploy\"}],
        \"volumes\":[
            {\"name\":\"feather-input\",\"configMap\":{\"name\":\"feather-input-${runid}\"}},
            {\"name\":\"feather-output\",\"persistentVolumeClaim\":{\"claimName\":\"feather-output-${runid}\"}}],
          \"containers\":[{
            \"name\":\"feather-service\",
            \"image\":\"norceresearch1/feather-service-epos-sra:2024.01\",
            \"volumeMounts\":[
              {\"name\":\"feather-input\",\"mountPath\":\"/app/input\"},
              {\"name\":\"feather-output\",\"mountPath\":\"/app/output\"}
            ]
          }]
        }}
  " \
  enlweb-feather-service

## Run a simple alpine image to copy the feather.txt file created as inpunt
## to the output directory and tar this file and the output stored on the
## PVC in the previous pod. The tarfile is streamed to stdout into the
## output directory of the parent pod.
echo "Obtain feather output."
kubectl -n swirrl run --quiet --restart=Never --image=alpine \
  --pod-running-timeout=30m \
  --overrides="
    {
      \"kind\":\"Pod\",\"apiVersion\":\"v1\",\"metadata\":{\"name\":\"feather-output-${runid}\"},
      \"spec\":{
        \"serviceAccount\":\"swirrl-wf-service-account\",
        \"volumes\":[
            {\"name\":\"feather-input\",\"configMap\":{\"name\":\"feather-input-${runid}\"}},
            {\"name\":\"feather-output\",\"persistentVolumeClaim\":{\"claimName\":\"feather-output-${runid}\"}}],
          \"containers\":[{
            \"name\":\"feather-output\",
            \"image\":\"alpine\",
            \"command\": [ \"sh\", \"-c\",
              \"cp -n /app/input/* /app/output && tar c -h -C /app/output/ -f /tmp/earthquakes.feather.tar . && sleep 30\"],
            \"volumeMounts\":[
              {\"name\":\"feather-input\",\"mountPath\":\"/app/input\"},
              {\"name\":\"feather-output\",\"mountPath\":\"/app/output\"}
            ]
          }]
        }}
  " \
  feather-output

kubectl -n swirrl wait --for=condition=Ready pod/feather-output-${runid}
kubectl -n swirrl cp feather-output-${runid}:/tmp/earthquakes.feather.tar /tmp/earthquakes.feather.tar
tar x -v -C /app/output -f /tmp/earthquakes.feather.tar
ls -l /app/output
declare -a outfiles
declare -a outfiles_urls
declare -a outfiles_missing
for outfile_url in $(ls -1 /app/output/*.__url__) ; do
  outfile=$(basename /app/output/${outfile_url} .__url__)
  echo "Testing if /app/output/${outfile} exists"
  if [ -e /app/output/${outfile} ] ; then
    outfiles+=(/app/output/${outfile})
    outfiles_urls+=(${outfile_url})
  else
    outfiles_missing+=(/app/output/${outfile})
  fi
done

function cleanup {
  runid=$1
  kubectl -n swirrl delete pod feather-output-${runid}
  kubectl -n swirrl delete pod feather-processing-${runid}
  kubectl -n swirrl delete pvc feather-output-${runid}
  kubectl -n swirrl delete configmap feather-input-${runid}
}

if [ ${#outfiles_missing[@]} -gt 0 ] ; then
  echo "Feather did not produce all expected output files."
  echo "Expected: ${expected_outputfiles[@]}"
  echo "Got: ${outfiles[@]}"
  echo "Missing: ${outfiles_missing[@]}"
fi

if [ ${#outfiles[@]} -eq 0 ] ; then
  echo "Feather did not produce any output."
  cleanup ${runid}
  exit 1
fi

echo "Stage the output."
# Stage the output
backup.sh /data/outputs/staginghistory/ ${outfiles[@]} ${outfiles_urls[@]}
cleanup ${runid}

exit 0
