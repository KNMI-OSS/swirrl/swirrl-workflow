FROM maven:3-amazoncorretto-17

RUN yum upgrade -y && yum update -y java-17-amazon-corretto

WORKDIR /usr/src/swirrl-workflow

COPY pom.xml /usr/src/swirrl-workflow
RUN mvn -Daether.connector.https.securityMode=insecure --batch-mode dependency:resolve
COPY . /usr/src/swirrl-workflow
RUN mvn -Daether.connector.https.securityMode=insecure --batch-mode package -Dmaven.test.skip=true

FROM amazoncorretto:17

RUN yum upgrade -y && yum update -y java-17-amazon-corretto && yum install shadow-utils -y && yum clean all

RUN useradd -d /home/swirrl -m -s /bin/bash swirrl
COPY --from=0 /usr/src/swirrl-workflow/target/swirrl-workflow-1.0.jar /usr/src/swirrl-workflow/
WORKDIR /usr/src/swirrl-workflow

USER swirrl
ENV HOME=/home/swirrl

ENTRYPOINT [ "java", "-jar", "swirrl-workflow-1.0.jar" ]
