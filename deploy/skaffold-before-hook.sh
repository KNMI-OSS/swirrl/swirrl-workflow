#!/bin/sh

DEPLOYMENT_PROPS_CONFIGMAP=swirrl-workflow-properties
DEPLOYMENT_PROPS_CONFIGMAP_FILENAME=swirrl-workflow.properties

kubectl -n ${NAMESPACE} create configmap ${DEPLOYMENT_PROPS_CONFIGMAP} \
  --from-file=${DEPLOYMENT_PROPS_CONFIGMAP_FILENAME} -o yaml --dry-run=client | kubectl --namespace ${NAMESPACE} apply -f -
