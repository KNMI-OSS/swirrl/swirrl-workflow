---
apiVersion: batch/v1
kind: Job
metadata:
  name: wf-wow-${USERID}-${SESSIONID}
spec:
  ## Leaving this out, sets it to 6 by default. Which I confirmed. At 0, it doesn't restart even once.
  ## However, even though restartPolicy is Never, the pod is terminated and logs are not available.
  ## According to https://kubernetes.io/docs/concepts/workloads/controllers/job/ they should be.
  backoffLimit: 0
  ## Set deadline to some time beyond half an hour (86400 seconds), which is the deadline the workflow
  ## dispatcher sets. This time includes the time a job's pod spends in Pending. It is the deadline
  ## for the job, the job's pod and/or container may fail and be restarted several times within this
  ## deadline, depending on backoffLimit setting.
  activeDeadlineSeconds: 2000
  template:
    metadata:
      name: wf-wow
    spec:
      initContainers: []
      # secret/deployment key specified by workflow.docker.image.pull.secret in application.properties
      # Value here provided for debugging purposes when using workflows/*/run.sh scripts.
      containers:
        # Location of the image in the gitlab registry. The API will override it with
        # workflow.docker.image.tag from application.properties.
        - image: ${CI_REGISTRY_IMAGE}/workflow/wow:${WF_WOW_IMAGE_TAG}
          # If running into problem K8S uses the old image:tag if image:tag is updated
          # uncomment following line to force download image. Can cause timeout-problems, so don't leave uncommented.
          # imagePullPolicy: Always
          name: cwl-wow-workspace-data
          args: ## mind the quotes
            - inputs.yml
          # Comment out args and uncomment command to get the contents of the container
          # in kubectl logs ${job-id}
          #  command: ["bash", "-c", "sleep 999"]
          resources:
            requests:
              memory: 2000M
              cpu: 500m
            limits:
              memory: 2000M
              cpu: 500m
          volumeMounts:
            - name: cwl-wow-persistent-storage
              mountPath: /data/outputs
            - name: configmap-inputs
              mountPath: /data/inputs
      restartPolicy: Never
      volumes:
        - name: cwl-wow-persistent-storage
          persistentVolumeClaim:
            # SWIRRL API overrides the name
            claimName: session-datadir-${USERID}-${SESSIONID}
        - name: configmap-inputs
          configMap:
            # SWIRRL API overrides the name
            name: cwl-wow-configmap-inputs-${USERID}-${SESSIONID}
