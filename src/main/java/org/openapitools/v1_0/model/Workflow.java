package org.openapitools.v1_0.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Workflow {
    @JsonProperty("sessionId")
    private String sessionId = null;

    @JsonProperty("inputs")
    private byte[] inputs = null;

    public Workflow sessionId(String sessionId) {
        this.sessionId = sessionId;
        return this;
    }

    /**
     * Get sessionId
     * @return sessionId
     **/
    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Workflow inputs(byte[] inputs) {
        this.inputs = inputs;
        return this;
    }

    public byte[] getInputs() {
        return inputs;
    }

    public void setInputs(byte[] inputs) {
        this.inputs = inputs;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Workflow workflow = (Workflow) o;
        return Objects.equals(this.sessionId, workflow.sessionId) &&
                Objects.equals(this.inputs, workflow.inputs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sessionId, inputs);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Workflow {\n");

        sb.append("    sessionId: ").append(toIndentedString(sessionId)).append("\n");
        sb.append("    inputs: ").append(toIndentedString(inputs)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
