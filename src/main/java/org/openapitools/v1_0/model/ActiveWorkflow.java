package org.openapitools.v1_0.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Objects;

public class ActiveWorkflow extends Workflow {

    static public String RUN_ACTIVITY_COMMAND = "run";
    static public String DELETE_SESSION_ACTIVITY_COMMAND = "delete-session";
    static public String DELETE_LATEST_STAGE_ACTIVITY_COMMAND = "delete-stage";
    static public String DELETE_CRON_WORKFLOW_COMMAND = "delete-cronworkflow";

    @JsonProperty("runId")
    private String runId;

    @JsonProperty("workflowName")
    private String workflowName;

    @JsonProperty("activity")
    private String activity;

    @JsonProperty("sessionHeader")
    private String sessionHeader;

    @JsonProperty("provenanceUser")
    private String provenanceUser;

    @JsonProperty("userName")
    private String userName;

    @JsonProperty("authMode")
    private String authMode;

    @JsonProperty("group")
    private String group;

    @JsonProperty("runAgent")
    private String runAgent;

    @JsonCreator
    public ActiveWorkflow(@JsonProperty("sessionId") String sessionId, @JsonProperty("inputs") byte[] inputs,
                          @JsonProperty("runId") String runId, @JsonProperty("workflowName") String workflowName,
                          @JsonProperty("activity") String activity, @JsonProperty("sessionHeader") String sessionHeader,
                          @JsonProperty("provenanceUser") String provenanceUser, @JsonProperty("userName") String userName,
                          @JsonProperty("authMode") String authMode, @JsonProperty("group") String group,
                          @JsonProperty("runAgent") String runAgent) {
        super.setSessionId(sessionId);
        super.setInputs(inputs);
        this.runId = runId;
        this.workflowName = workflowName;
        this.activity = activity;
        this.sessionHeader = sessionHeader;
        this.provenanceUser = provenanceUser;
        this.userName = userName;
        this.authMode = authMode;
        this.group = group;
        this.runAgent = runAgent;
    }

    public String getRunId() {
        return runId;
    }

    public void setRunId(String runId) {
        this.runId = runId;
    }

    public String getWorkflowName() {
        return workflowName;
    }

    public void setWorkflowName(String workflowName) {
        this.workflowName = workflowName;
    }

    public String getActivity() { return activity; }

    public void setActivity(String activity) { this.activity = activity; }

    public String getSessionHeader() { return sessionHeader; }

    public void setSessionHeader(String sessionHeader) { this.sessionHeader = sessionHeader; }

    public String getProvenanceUser() { return provenanceUser; }

    public void setProvenanceUser(String provenanceUser) { this.provenanceUser = provenanceUser; }

    public String getUserName() { return userName; }

    public void setUserName(String userName) { this.userName = userName; }

    public String getAuthMode() { return authMode; }

    public void setAuthMode(String authMode) { this.authMode = authMode; }

    public String getGroup() { return group; }

    public void setGroup(String group) { this.group = group; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ActiveWorkflow that = (ActiveWorkflow) o;
        return Objects.equals(runId, that.runId) && Objects.equals(workflowName, that.workflowName) && Objects.equals(activity, that.activity) && Objects.equals(sessionHeader, that.sessionHeader) && Objects.equals(provenanceUser, that.provenanceUser) && Objects.equals(userName, that.userName) && Objects.equals(authMode, that.authMode) && Objects.equals(group, that.group) && Objects.equals(runAgent, that.runAgent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), runId, workflowName, activity, sessionHeader, provenanceUser, userName, authMode, group, runAgent);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString()
                .replace("class Workflow ", "class ActiveWorkflow ")
                .replace("}",""));
        sb.append("    runId: ").append(runId);
        sb.append("}");
        return sb.toString();
    }

    public String getRunAgent() {
        return runAgent;
    }

    public void setRunAgent(String runAgent) {
        this.runAgent = runAgent;
    }
}
