package nl.knmi.swirrl.workflow.provenance;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.kubernetes.client.openapi.models.V1Pod;
import io.kubernetes.client.openapi.models.V1PodList;
import nl.knmi.CreateWorkflowBindingsBean;
import nl.knmi.FailWorkflowBindingsBean;
import nl.knmi.RollbackWorkflowBindingsBean;
import nl.knmi.swirrl.workflow.SwirrlApiConfiguration;
import nl.knmi.swirrl.workflow.TemplateCatalog;
import nl.knmi.swirrl.workflow.kubernetes.KubernetesService;
import nl.knmi.swirrl.workflow.model.Job;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.HttpResponse;
import org.apache.hc.core5.http.HttpStatus;
import org.apache.hc.core5.http.ParseException;
import org.apache.hc.core5.http.io.entity.StringEntity;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.net.URIBuilder;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openapitools.v1_0.model.ActiveWorkflow;
import org.openprovenance.prov.model.ProvFactory;
import org.openprovenance.prov.template.expander.Bindings;
import org.openprovenance.prov.template.expander.BindingsJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import org.springframework.stereotype.Service;

import javax.json.*;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@EnableConfigurationProperties(value = {ProvenanceConfiguration.class, SwirrlApiConfiguration.class})
public class WorkflowProvenanceService {

    private Logger logger = LoggerFactory.getLogger(WorkflowProvenanceService.class);

    private KubernetesService kubernetesService;

    private final ProvenanceConfiguration provenanceConfiguration;
    private final SwirrlApiConfiguration swirrlApiConfiguration;
    private TemplateCatalog templateCatalog;

    private final String UUID_NAMESPACE = "urn:uuid:";
    private final String UUID_PREFIX = "uuid";
    public final static String DATADIRECTORY_PVC_NAME_PREFIX = "data-directory-";

    @Value("${provenance.notebookapi.k8s_recipe.id}")
    private String NOTEBOOK_API_K8S_RECIPE_ID;

    @Autowired
    public WorkflowProvenanceService(KubernetesService kubernetesService,
                                     ProvenanceConfiguration provenanceConfiguration,
                                     SwirrlApiConfiguration swirrlApiConfiguration,
                                     TemplateCatalog templateCatalog) {
        this.kubernetesService = kubernetesService;
        this.provenanceConfiguration = provenanceConfiguration;
        this.swirrlApiConfiguration = swirrlApiConfiguration;
        this.templateCatalog = templateCatalog;
    }

    public String traceProvenance(ActiveWorkflow workflow, Job job) {
        ProvFactory provFactory = org.openprovenance.prov.xml.ProvFactory.getFactory();
        CreateWorkflowBindingsBean bindingsBean = new CreateWorkflowBindingsBean(provFactory);
        bindingsBean.addFilelabel("filename");

        bindingsBean.addWorkflowid(provFactory.newQualifiedName(UUID_NAMESPACE, job.getImageID().replaceAll("[^a-zA-Z0-9]+", "-"), UUID_PREFIX));
        bindingsBean.addSystemimagelocation(job.getImageID());
        bindingsBean.addStarttime(job.getStartTime());
        bindingsBean.addEndtime(job.getEndTime());
        bindingsBean.addSessionid(workflow.getSessionId());
        bindingsBean.addWfrun(provFactory.newQualifiedName(UUID_NAMESPACE, workflow.getRunId(), UUID_PREFIX));
        bindingsBean.addJobid(job.getId());
        bindingsBean.addWorkflowname(workflow.getWorkflowName());

        // Initialize User
        bindingsBean.addUser(provFactory.newQualifiedName(UUID_NAMESPACE, workflow.getUserName(), UUID_PREFIX));
        bindingsBean.addAuthmode(workflow.getAuthMode());
        bindingsBean.addGroup(workflow.getGroup());
        bindingsBean.addName(workflow.getProvenanceUser());

        // Initialize NotebookAPI
        bindingsBean.addMethodpath("POST /workflow/"+workflow.getWorkflowName()+"/run/");

        if (workflow.getRunAgent().equalsIgnoreCase("SWIRRL-API"))
            bindingsBean.addNameapi("SWIRRL-API");
        else
            bindingsBean.addNameapi("swirrl-cron-"+workflow.getWorkflowName());

        bindingsBean.addRunagent(provFactory.newQualifiedName(UUID_NAMESPACE, workflow.getRunAgent(), UUID_PREFIX));

        // Initialize volumes
        String datadirVolumeName = DATADIRECTORY_PVC_NAME_PREFIX + workflow.getRunId();
        bindingsBean.addVolume(provFactory.newQualifiedName(UUID_NAMESPACE, datadirVolumeName, UUID_PREFIX));
        String body = String.format("[{\"op\": \"add\",\"path\": \"/metadata/labels/volumeid\",\"value\": \""+datadirVolumeName+"\"}]");
        kubernetesService.patchNamedPvcBySession(workflow.getSessionId(), datadirVolumeName, body);
        bindingsBean.addVolumeid(datadirVolumeName);

        // When in the first stage, the data volume should reference to the initial created volume
        String prevVolume = DATADIRECTORY_PVC_NAME_PREFIX + workflow.getSessionId();
        ArrayList<String> runIds = getLatestRunIds(getLatestActivityIds(workflow.getSessionId()));
        runIds.add(workflow.getRunId());
        if (runIds.size() > 1) {
            boolean foundPrevVolume = false;
            for (int i = runIds.size() - 2; i >= 0; i--) {
                String prevRunId = runIds.get(i);
                try {
                    HttpGet request = new HttpGet("http://"+swirrlApiConfiguration.getServicename()+":"+
                            swirrlApiConfiguration.getServiceport()+swirrlApiConfiguration.getContextpath()+"/"+
                            swirrlApiConfiguration.getVersion()+"/provenance/activity/urn:uuid:"+prevRunId);
                    CloseableHttpClient httpClient = HttpClients.createDefault();
                    CloseableHttpResponse response = httpClient.execute(request);
                    if (HttpStatus.SC_OK != response.getCode()) {
                        logger.error("Failed to retrieve provenance.\n"+response.getCode());
                        continue;
                    }
                    HttpEntity entity = response.getEntity();
                    String jsonString = EntityUtils.toString(entity);
                    JsonReader jsonReader = Json.createReader(new StringReader(jsonString));
                    JsonObject jsonObject = jsonReader.readObject();
                    JsonArray members = jsonObject.getJsonObject("prov:generated").getJsonArray("prov:hadMember");
                    for (JsonValue member : members) {
                        JsonObject typesObject = member.asJsonObject();
                        JsonArray types = typesObject.getJsonArray("@type");
                        for (int k = 0; k < types.size(); k++) {
                            if (types.getString(k).equals("swirrl:Storage")) {
                                if (!typesObject.containsKey("prov:wasInvalidatedBy")) {
                                    prevVolume = DATADIRECTORY_PVC_NAME_PREFIX + prevRunId;
                                    foundPrevVolume = true;
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    logger.error("Failed to retrieve provenance.\n"+e.getMessage());
                }
                if (foundPrevVolume) {
                    break;
                }
            }
        }
        bindingsBean.addVolumeprev(provFactory.newQualifiedName(UUID_NAMESPACE, prevVolume, UUID_PREFIX));
        bindingsBean.addGeneratedat(job.getEndTime());

        // Initialize work flow input
        bindingsBean.addParname("in");
        byte[] inputsDecoded = workflow.getInputs();
        try {
            String inputs = new String(inputsDecoded, "ASCII");
            bindingsBean.addParvalue(inputs);
        } catch (UnsupportedEncodingException e) { ; }

        String logString = new String(job.getLogs(), Charset.defaultCharset());
        List<JsonNode> jsonObjects = this.parseJobLogs(logString);
        /*
        The download workflow has two json objects in the job log, the contents of the file swirrl_fileinfo.json and the
        output of the cwl.
        For now we need the swirrl_fileinfo to get the status and ID of each file.
        The file state can be:
            - new  --> "hadMember".
            - updated --> "hadMember" + "wasDerivedFrom"
            - unchanged: --> reference to previous (info not yet available)

        Fetch the jsonObject containing the file meta data. This json object should have the keys "stageNumber" and "files".
        A file in "files" has one of the following states:
        Iterator<String> foreach = arr.iterator();
        while (foreach.hasNext()) System.out.println(foreach.next());
         */
        Iterator<JsonNode> iterator = jsonObjects.iterator();
        while (iterator.hasNext()) {
            JsonNode jsonObj = iterator.next();
            if (jsonObj == null) continue; // shouldn't happen anymore, but better safe than sorry.
            logger.debug("Investigate json: " + jsonObj.toString() );
            logger.debug("create file entities in template: " + bindingsBean.getTemplate());
            if (jsonObj.has("stageNumber") && jsonObj.has("files")) {
                // jsonObj is swirrl_fileinfo.json
                String stagePath = jsonObj.get("stagePath").toString().replace("\"", "");
                Iterator fileIterator = jsonObj.get("files").elements();
                while (fileIterator.hasNext()) {
                    JsonNode fileObj = (JsonNode)fileIterator.next();
                    bindingsBean.addFile(provFactory.newQualifiedName(UUID_NAMESPACE, fileObj.get("id").toString().replace("\"", ""), UUID_PREFIX));
                    bindingsBean.addBasename(fileObj.get("filename").toString().replace("\"", ""));
                    bindingsBean.addPath(stagePath);
                    bindingsBean.addAtlocation(fileObj.get("sourceUrl").toString().replace("\"", ""));
                    String fileState = fileObj.get("state").toString().replace("\"", "");
                    if ("new".equals(fileState)){
                        logger.debug("New File " + fileObj.get("id").toString().replace("\"", "") + " is created");
                        bindingsBean.addFileprev(provFactory.newQualifiedName(UUID_NAMESPACE, "nullFile", UUID_PREFIX));
                    }
                    if ("updated".equals(fileState)){
                        logger.debug("updated File adding FilePrev " );
                        bindingsBean.addFileprev(provFactory.newQualifiedName(UUID_NAMESPACE, fileObj.get("prevId").toString().replace("\"", ""), UUID_PREFIX));
                    }
                    if ("unchanged".equals(fileState)){
                        logger.debug("unchanged File: adding current ID as FilePrev  ");
                        bindingsBean.addFileprev(provFactory.newQualifiedName(UUID_NAMESPACE, fileObj.get("id").toString().replace("\"", ""), UUID_PREFIX));
                    }
                }
            }
        }

        bindingsBean.addMessage(job.getStatus());

        // Export as json
        Bindings bindings = bindingsBean.getBindings();
        bindings.addVariableBindingsAsAttributeBindings();
        BindingsJson.BindingsBean bean = BindingsJson.toBean(bindings);

        ObjectMapper mapper = new ObjectMapper();
        String resultBindings;

        try {
            resultBindings = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(bean);
            logger.info(resultBindings);
        } catch (IOException e) {
            logger.error("Could not trace provenance for workflow "+ job.getId());
            return null;
        }

        return resultBindings;
    }

    public String traceFailRunWorkflow(ActiveWorkflow workflow, Job job) {
        String sessionId = workflow.getSessionId();
        ProvFactory provFactory = org.openprovenance.prov.xml.ProvFactory.getFactory();
        FailWorkflowBindingsBean bindingsBean = new FailWorkflowBindingsBean(provFactory);

        bindingsBean.addJobid(job.getId());
        bindingsBean.addSessionid(sessionId);
        bindingsBean.addWorkflowid(provFactory.newQualifiedName(UUID_NAMESPACE, job.getImageID().replaceAll("[^a-zA-Z0-9]+", "-"), UUID_PREFIX));
        bindingsBean.addSystemimagelocation(job.getImageID());
        bindingsBean.addWfrun(provFactory.newQualifiedName(UUID_NAMESPACE, workflow.getRunId(), UUID_PREFIX));
        bindingsBean.addWorkflowname(workflow.getWorkflowName());

        // Initialize User
        bindingsBean.addUser(provFactory.newQualifiedName(UUID_NAMESPACE, workflow.getUserName(), UUID_PREFIX));
        bindingsBean.addAuthmode(workflow.getAuthMode());
        bindingsBean.addGroup(workflow.getGroup());
        bindingsBean.addName(workflow.getProvenanceUser());

        bindingsBean.addStarttime(job.getStartTime());
        bindingsBean.addEndtime(DateTime.now().toString());
        bindingsBean.addMethodpath("POST /workflow/"+workflow.getWorkflowName()+"/run/");
        logger.debug("Failed workflow triggered by %s"+workflow.getRunAgent());
        bindingsBean.addRunagent(provFactory.newQualifiedName(UUID_NAMESPACE, workflow.getRunAgent(), UUID_PREFIX));
        bindingsBean.addNameapi("SWIRRL-API");

        bindingsBean.addParname("in");
        byte[] inputsDecoded = workflow.getInputs();
        try {
            String inputs = new String(inputsDecoded, "ASCII");
            bindingsBean.addParvalue(inputs);
        } catch (UnsupportedEncodingException e) { ; }

        bindingsBean.addMessage("ERROR: "+job.getStatus());

        // Export as json
        Bindings bindings = bindingsBean.getBindings();
        bindings.addVariableBindingsAsAttributeBindings();
        BindingsJson.BindingsBean bean = BindingsJson.toBean(bindings);

        ObjectMapper mapper = new ObjectMapper();
        String resultBindings;

        try {
            resultBindings = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(bean);
            logger.info(resultBindings);
        } catch (IOException e) {
            logger.error("Could not trace provenance for workflow "+ job.getId());
            return null;
        }

        return resultBindings;

    }


    public String traceProvenanceDeleteLatestStage(ActiveWorkflow workflow, Job job) {
        String sessionId = workflow.getSessionId();
        ProvFactory provFactory = org.openprovenance.prov.xml.ProvFactory.getFactory();
        RollbackWorkflowBindingsBean bindingsBean = new RollbackWorkflowBindingsBean(provFactory);
        String imageID;
        V1PodList podList = kubernetesService.retrievePodsByLabel("dls-jobid="+job.getId());
        V1Pod pod = podList.getItems().get(0);

        if (null != pod) {
            // ISSUE: No image Id can be retreived from pod:
            imageID = pod.getStatus().getContainerStatuses().get(0).getImageID();
        }
        else {
            logger.warn("Could not determine imageID, perhaps the pod crashed. Session: "+sessionId+", job: "+job.getId());
            imageID = "COULD_NOT_BE_DETERMINED";
        }
        bindingsBean.addImagelabel("Rollback");

        bindingsBean.addJobid(job.getId());
        bindingsBean.addSessionid(sessionId);
        bindingsBean.addSystemimage(provFactory.newQualifiedName(UUID_NAMESPACE, imageID.replaceAll("[^a-zA-Z0-9]+", "-"), UUID_PREFIX));
        bindingsBean.addSystemimagelocation(imageID);

        // Initialize User
        bindingsBean.addUser(provFactory.newQualifiedName(UUID_NAMESPACE, workflow.getUserName(), UUID_PREFIX));
        bindingsBean.addAuthmode(workflow.getAuthMode());
        bindingsBean.addGroup(workflow.getGroup());
        bindingsBean.addName(workflow.getProvenanceUser());

        bindingsBean.addNameapi("SWIRRL-API");
        bindingsBean.addRunagent(provFactory.newQualifiedName(UUID_NAMESPACE, "v1.0", UUID_PREFIX));
        bindingsBean.addMethodpath("DEL /session/"+sessionId+"/lateststage");
        bindingsBean.addStarttime(job.getStartTime());
        bindingsBean.addEndtime(DateTime.now().toString());

        bindingsBean.addRollback(provFactory.newQualifiedName(UUID_NAMESPACE, UUID.randomUUID().toString(), UUID_PREFIX));
        /*
         The volume must be postfixed with the stage number to be correct. But
         DeleteSessionLatestStage end point doesn't wait for the job to finish and
         until now, the job didn't return the latest stage. So we have no way to
         determine this at the moment, until we move the implementation of this
         end point to swirrl-workflow and run it from a queue so we can wait for
         the job to finish.
         */
        String logString = new String(job.getLogs(), Charset.defaultCharset());
        logger.debug("LOGS: "+logString);
        String logLines[] = logString.split("\\n");
        Pattern pattern = Pattern.compile("stage \\d+ from run (.*) deleted from staging history");
        String runId = null;
        for (String logLine : logLines) {
            Matcher matcher = pattern.matcher(logLine);
            if (matcher.find()) {
                runId = matcher.group(1);
            }
        }

        String datadirVolumeName = DATADIRECTORY_PVC_NAME_PREFIX + runId;
        bindingsBean.addVolume(provFactory.newQualifiedName(UUID_NAMESPACE, datadirVolumeName, UUID_PREFIX));

        // Export as json
        Bindings bindings = bindingsBean.getBindings();
        bindings.addVariableBindingsAsAttributeBindings();
        BindingsJson.BindingsBean bean = BindingsJson.toBean(bindings);

        ObjectMapper mapper = new ObjectMapper();
        String resultBindings;
        try {
            resultBindings = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(bean);
            logger.info(resultBindings);
        } catch (IOException e) {
            logger.error("Could not trace provenance for workflow "+ job.getId());
            return null;
        }

        return resultBindings;
    }

    public String extractProvenanceFromLogs(ActiveWorkflow workflow, Job job) {
        String logs = new String(job.getLogs(), Charset.defaultCharset());
        logger.debug("Looking for prov document in job logs");
        Pattern pattern = Pattern.compile("^(document$.*?^endDocument)$", Pattern.DOTALL | Pattern.MULTILINE);
        Matcher m = pattern.matcher(logs);
        if (m.find()) {
            String provDoc = m.group(1);
            logger.debug("FOUND: " + provDoc);
            List<JsonNode> jsonObjects = this.parseJobLogs(logs);
            for (JsonNode jsonObj : jsonObjects) {
                if (null == jsonObj) continue;  // shouldn't happen anymore, but better safe than sorry.
                logger.debug("Investigate json: " + jsonObj.toString());
                if (jsonObj.has("stageNumber") && jsonObj.has("files")) {
                    // jsonObj is swirrl_fileinfo.json
                    Iterator<JsonNode> fileIterator = jsonObj.get("files").elements();
                    while (fileIterator.hasNext()) {
                        JsonNode fileObj = fileIterator.next();
                        String filename = fileObj.get("filename").toString().replace("\"", "");
                        String fileid = fileObj.get("id").toString().replace("\"", "");
                        logger.debug("Replacing <" + filename + "> with " + fileid);
                        provDoc = provDoc.replaceAll("<" + filename + ">", fileid);
                    }
                }
            }
            provDoc = provDoc.replaceAll("<run_id>", workflow.getRunId());
            logger.debug(provDoc);
            return provDoc;
        }
        return null;
    }

    private List<JsonNode> parseJobLogs(String logs) {

        logger.debug("RAW LOGS: "+logs);
        String cwlOutputJson = new String();
        Pattern pattern = Pattern.compile("^(\\{$.*?^\\})$", Pattern.DOTALL | Pattern.MULTILINE);
        Matcher m = pattern.matcher(logs);
//        In Download CWL the first json is "swirrl_info.json", the second is output of cwl itself.
//        We do need swirrl_info.json.
        List<JsonNode> jsonObjects = new ArrayList<JsonNode>();
        while (m.find()) {
            cwlOutputJson = m.group(1);
            logger.debug("FOUND: " + cwlOutputJson);
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonObj = null;
            try {
                jsonObj = mapper.readTree(cwlOutputJson);
                jsonObjects.add(jsonObj);
            } catch (IOException exception) {
                logger.debug("Exception deserializing JSON part of CWL logs.");
            }
        }
        logger.debug("JSON part: "+cwlOutputJson);

        return jsonObjects;
    }

    /**
     * Expands a template using the template expansion service.
     * @param resultBindings The bindings used for the expansion. This should be v3 json bindings.
     * @return A response indicating if the expansion was successful. If it was successful, the body contains the expanded PROV document.
     */
    public String expandProvTemplate(String resultBindings, String templateName) {
        String result = null;
        String templateId = templateCatalog.getIdByTitle(templateName);
        final HttpPost request = new HttpPost(
                provenanceConfiguration.getTemplate().getExpansionUrl()+ "/templates/" + templateId
                        + "/expand?fmt=provn&writeprov=false&bindver=v3");
        request.setEntity(new StringEntity(resultBindings));
        try (final CloseableHttpClient httpclient = HttpClients.createDefault()) {
            try (CloseableHttpResponse response = httpclient.execute(request)) {
                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity);
            }
        } catch (IOException | ParseException e) {
            logger.error("Failed to expand bindings." + e.getMessage());
        }
        return result;
    }

    public HttpResponse saveProvenanceSession(String expandedBindings){
        logger.debug("Saving provenance.");
        final HttpPost request = new HttpPost("http://"+swirrlApiConfiguration.getServicename()+":"+
                swirrlApiConfiguration.getServiceport()+swirrlApiConfiguration.getContextpath()+"/"+
                swirrlApiConfiguration.getVersion()+"/provenance");
        request.setEntity(new StringEntity(expandedBindings));
        request.addHeader("Content-Type", "application/octet-stream");
        try (final CloseableHttpClient httpclient = HttpClients.createDefault()) {
            CloseableHttpResponse response = httpclient.execute(request);
            return response;
        } catch (IOException e) {
            logger.error("Failed to expand bindings." + e.getMessage());
        }

        return null;
    }

    public ArrayList<String> getLatestActivityIds( String sessionId) {
        logger.debug("Fetching activity IDs for session "+sessionId);
        try {
            final HttpGet request = new HttpGet("http://"+swirrlApiConfiguration.getServicename()+":"+
                    swirrlApiConfiguration.getServiceport()+swirrlApiConfiguration.getContextpath()+"/"+
                    swirrlApiConfiguration.getVersion()+"/provenance/session/"+sessionId+"/activities");
            try {
                URI uri = new URIBuilder(request.getUri())
                        .addParameter("includeRuns", "true")
                        .addParameter("includeAdditionalInfo", "false").build();
                request.setUri(uri);
            } catch (URISyntaxException e) { }

            ArrayList<String> activityIds = new ArrayList<>();
            try (CloseableHttpClient client = HttpClients.createDefault()) {
                CloseableHttpResponse response = client.execute(request);
                if (HttpStatus.SC_OK != response.getCode()) {
                    logger.debug("No activity IDs for session {}", sessionId);
                    return activityIds; // empty list.
                }
                String activityList = EntityUtils.toString(response.getEntity());
                JSONObject sessionActivityJSON = new JSONObject(activityList);
                JSONArray activities = sessionActivityJSON.getJSONArray("@graph");

                for (int i = 0; i < activities.length(); i++) {
                    JSONArray activityType = activities.getJSONObject(i).getJSONArray("@type");
                    for (int j = 0; j < activityType.length(); j++) {
                        if (activityType.getString(j).equals("swirrl:RunWorkflow")
                                && activities.getJSONObject(i).has("@id")) {
                            String activityId = activities.getJSONObject(i).getString("@id");
                            activityIds.add(activityId);
                        }
                    }
                }
            } catch (IOException | ParseException e) {
                logger.error("Could not obtain activity IDs");
            }

            return activityIds;

        } catch (JSONException e) {
            logger.error("Exception in parsing activity list:", e);
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<String> getLatestRunIds( ArrayList<String> activityIds) {
        ArrayList<String> runIds = new ArrayList<>();

        for (String activityId : activityIds) {
            try {
                final HttpGet request = new HttpGet("http://"+swirrlApiConfiguration.getServicename()+":"+
                        swirrlApiConfiguration.getServiceport()+swirrlApiConfiguration.getContextpath()+"/"+
                        swirrlApiConfiguration.getVersion()+"/provenance/activity/"+activityId);
                try (CloseableHttpClient client = HttpClients.createDefault()) {
                    CloseableHttpResponse response = client.execute(request);
                    if (response.getCode() == HttpStatus.SC_OK) {
                        String activityDoc = EntityUtils.toString(response.getEntity());
                        JSONObject activityObj = new JSONObject(activityDoc);

                        String runId = activityObj.getString("swirrl:jobId");
                        runIds.add(runId);
                    } else {
                        logger.debug("Could not get activity document for activity {}", activityId);
                    }
                } catch (IOException e) {
                    logger.error("Could not obtain activity IDs");
                }
            } catch (JSONException | ParseException e) {
                logger.error("Exception in getting run ids:", e);
                e.printStackTrace();
                return null;
            }
        }
        return  runIds;
    }
}
