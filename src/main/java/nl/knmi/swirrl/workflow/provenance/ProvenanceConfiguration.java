package nl.knmi.swirrl.workflow.provenance;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix="provenance")
public class ProvenanceConfiguration {
    private final Template template = new Template();

    public Template getTemplate() {
        return template;
    }

    public NotebookApi getNotebookApi() {
        return notebookApi;
    }

    private final NotebookApi notebookApi = new NotebookApi();

    public class Template {
        private String expansionUrl;
        private String runWorkflowId;

        public String getExpansionUrl() {
            return expansionUrl;
        }

        public void setExpansionUrl(String expansionUrl) {
            this.expansionUrl = expansionUrl;
        }

        public String getRunWorkflowId() {
            return runWorkflowId;
        }

        public void setRunWorkflowId(String runWorkflowId) {
            this.runWorkflowId = runWorkflowId;
        }
    }

    public class NotebookApi {
        private String Id;
        private String k8sRecipeId;

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getK8sRecipeId() {
            return k8sRecipeId;
        }

        public void setK8sRecipeId(String k8sRecipeId) {
            this.k8sRecipeId = k8sRecipeId;
        }
    }
}
