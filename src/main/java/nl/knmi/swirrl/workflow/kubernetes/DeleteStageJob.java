package nl.knmi.swirrl.workflow.kubernetes;
import io.kubernetes.client.openapi.models.V1ConfigMap;
import io.kubernetes.client.openapi.models.V1Job;

import java.util.Objects;

public class DeleteStageJob {
    private String sessionId;
    private V1ConfigMap rollbackConfigMap;
    private String datadirPvcName;
    private V1Job job;
 

    public DeleteStageJob(String sessionId, V1ConfigMap rollbackConfigMap, String datadirPvcName, V1Job job) {
        super();
        this.sessionId = sessionId;
        this.rollbackConfigMap = rollbackConfigMap;
        this.datadirPvcName = datadirPvcName;
        this.job = job;
    }

    @Override
    public String toString() {
        return "DeleteStageJob [rollbackConfigMap=" + rollbackConfigMap + ", datadirPvc=" + datadirPvcName + ", job=" + job + "]";
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        DeleteStageJob other = (DeleteStageJob) obj;
        return Objects.equals(rollbackConfigMap, other.rollbackConfigMap) &&
                Objects.equals(datadirPvcName, other.datadirPvcName) &&
                Objects.equals(job, other.job);
    }

    public V1ConfigMap getRollbackConfigMap() {
        return rollbackConfigMap;
    }
    public void setRollbackConfigMap(V1ConfigMap rollbackConfigMap) {
        this.rollbackConfigMap = rollbackConfigMap;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getDatadirPvcName() {
    return datadirPvcName;
    }
    public void setDatadirPvcName(String datadirPvcName) {
        this.datadirPvcName = datadirPvcName;
    }
    public V1Job getJob() {
        return job;
    }
    public void setJob(V1Job job) {
        this.job = job;
    }
}
