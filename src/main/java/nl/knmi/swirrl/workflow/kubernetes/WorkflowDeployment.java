package nl.knmi.swirrl.workflow.kubernetes;

import io.kubernetes.client.openapi.models.V1ConfigMap;

import java.util.Objects;

public class WorkflowDeployment<T> {

    private String runId;
    private V1ConfigMap inputsConfigMap;
    private String datadirPvcName;
    private T job;

    protected WorkflowDeployment(String runId, V1ConfigMap inputsConfigMap,
                                 String datadirPvcName, T job) {
        super();
        this.runId = runId;
        this.inputsConfigMap = inputsConfigMap;
        this.datadirPvcName = datadirPvcName;
        this.job = job;
    }

    public String getRunId() {
        return runId;
    }

    public void setRunId(String runId) {
        this.runId = runId;
    }

    @Override
    public String toString() {
        return "WorkflowDeployment [inputsConfigMap=" + inputsConfigMap
                + ", datadirPvc=" + datadirPvcName + ", job=" + job + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        WorkflowDeployment other = (WorkflowDeployment) obj;
        return Objects.equals(inputsConfigMap, other.inputsConfigMap) &&
                Objects.equals(datadirPvcName, other.datadirPvcName) &&
                Objects.equals(job, other.job);
    }

    public V1ConfigMap getInputsConfigMap() {
        return inputsConfigMap;
    }
    public void setInputsConfigMap(V1ConfigMap inputsConfigMap) {
        this.inputsConfigMap = inputsConfigMap;
    }
    public String getDatadirPvcName() {
        return datadirPvcName;
    }
    public void setDatadirPvcName(String datadirPvcName) {
        this.datadirPvcName = datadirPvcName;
    }
    public T getJob() {
        return job;
    }
    public void setJob(T job) {
        this.job = job;
    }

}
