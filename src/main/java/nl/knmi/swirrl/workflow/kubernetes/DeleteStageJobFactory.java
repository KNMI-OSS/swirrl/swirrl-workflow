package nl.knmi.swirrl.workflow.kubernetes;

import io.kubernetes.client.openapi.models.*;
import io.kubernetes.client.util.Yaml;
import nl.knmi.swirrl.workflow.SwirrlApiConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

@Service
public class DeleteStageJobFactory {

    private static Logger logger = LoggerFactory.getLogger(DeleteStageJobFactory.class);

    @Value("${session.deletestage.image.tag}")
    private String dockerImageTag;
    @Value("${session.deletestage.image.pull.secret}")
    private String dockerImagePullSecret;

    public final static String DELETESTAGE_PREFIX = "dls";
    public final static String DELETESTAGE_SESSION_ID_LABEL = WorkflowDeploymentFactory.SESSION_ID_LABEL + "-deleteStage";
    public final static String DATADIRECTORY_PVC_NAME_PREFIX = "data-directory-";
    public final static String JOB_ID_LABEL = DELETESTAGE_PREFIX+"-jobid";

    public static String determineAppName(String sessionId) {
        return DELETESTAGE_PREFIX + "-" + sessionId;
    }

    public DeleteStageJob makeRollbackDeployment(String sessionId, ArrayList<String> runIds, String jobId) {
        final String appName = determineAppName(sessionId);
        final String datadirPvcName = DATADIRECTORY_PVC_NAME_PREFIX + sessionId;

        final Map<String, String> appLabels = Map.of(
                "app", appName,
                "type", "deletestage",
                DELETESTAGE_SESSION_ID_LABEL, sessionId,
                JOB_ID_LABEL, jobId);

        V1ConfigMap rollbackConfigMap = buildRollbackConfigMap(runIds, sessionId, appLabels);
        V1Job job = buildDeleteStageJob(appName, appLabels, datadirPvcName,
                rollbackConfigMap.getMetadata().getName(), sessionId);
        DeleteStageJob deployment = new DeleteStageJob(sessionId, rollbackConfigMap, datadirPvcName, job);

        return deployment;
    }

    private String determineImageTag() {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("/home/swirrl/swirrl-workflow.properties"));
        } catch (FileNotFoundException e) {
            return dockerImageTag;
        } catch (IOException e) {
            e.printStackTrace();
            return dockerImageTag;
        }
        if (properties.containsKey("session.deletestage.image.tag"))
            return properties.getProperty("session.deletestage.image.tag");
        else return dockerImageTag;
    }

    private V1Job buildDeleteStageJob(String appName, Map<String, String> appLabels,  String datadirPvcName, String rollbackConfigMapName, String sessionId) {

        V1Job job = null;
        try {
            job = Yaml.loadAs(new InputStreamReader(
                    new ClassPathResource("/k8s-specs/rollback/delete-stage.yaml").getInputStream()), V1Job.class);
        } catch (final IOException e) {
            logger.error("Exception occurred whilst reading yaml file.", e);
            return null;
        }
        final String jobTimestamp = new SimpleDateFormat("yyyyMMddHHmmSS").format(new Date());

        V1LocalObjectReference dockerImagePullSecretObj = new V1LocalObjectReference();
        dockerImagePullSecretObj.setName(dockerImagePullSecret);

        job = new V1JobBuilder(job)
                .editMetadata().withName(appName + "-" + jobTimestamp).withLabels(appLabels).endMetadata()
                .editSpec().editTemplate().editMetadata().withName(appName).withLabels(appLabels).endMetadata()
                .editSpec()
                .withImagePullSecrets(dockerImagePullSecretObj)
                .endSpec()
                .editSpec().editMatchingContainer(item -> "job-delete-stage".equals(item.getName()))
                .withImage(determineImageTag())
                .editMatchingVolumeMount(item -> item.getName().equals("data"))
                .withSubPath(sessionId)
                .endVolumeMount()
                .endContainer()
                .editMatchingVolume(item -> "data".equals(item.getName()))
                .editPersistentVolumeClaim()
                .withClaimName(datadirPvcName)
                .endPersistentVolumeClaim()
                .endVolume()
                .editMatchingVolume(item -> "configmap-rollback".equals(item.getName()))
                .editConfigMap().withName(rollbackConfigMapName)
                .endConfigMap()
                .endVolume()
                .endSpec().endTemplate().endSpec()
                .build();
        logger.debug(Yaml.dump(job));
        return job;
    }


    public static V1ConfigMap buildRollbackConfigMap(ArrayList<String> runId, String sessionId, Map<String, String> appLabels) {
        V1ConfigMap rollbackConfigMap = new V1ConfigMapBuilder()
                .withNewMetadata().withName(DELETESTAGE_PREFIX+"-"+sessionId+"-rollback").withLabels(appLabels).endMetadata()
                .withData(Map.of("metadata.txt", runId.toString()))
                .build();

        return rollbackConfigMap;
    }
}