package nl.knmi.swirrl.workflow.kubernetes;

import io.kubernetes.client.openapi.models.*;
import io.kubernetes.client.util.Yaml;
import org.openapitools.v1_0.model.ActiveWorkflow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.yaml.snakeyaml.constructor.ConstructorException;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;


@Service
public class WorkflowDeploymentFactory {
    private static Logger logger = LoggerFactory.getLogger(WorkflowDeploymentFactory.class);

    public final static String SESSION_ID_LABEL = "session-id";
    public final static String WORKFLOW_PREFIX = "cwl";
    public final static String WORKFLOW_SESSION_ID_LABEL = SESSION_ID_LABEL + "-workflow";
    public final static String TOKEN_ENDPOINT = "http://internal-server.c4i.svc.cluster.local:4000/access_token/";
    public final static String DATADIRECTORY_PVC_NAME_PREFIX = "data-directory-";
    public final static String RUN_ID_LABEL = "run-id";
    private static final String CUSTOM_WORKFLOW_CONFIGMAP_NAME = "custom-workflows";

    private final Map<String, String> dockerImageTagMap;
    private final String dockerImagePullSecret;
    private final KubernetesService kubernetesService;

    @Autowired
    public WorkflowDeploymentFactory(
            @Value("#{${workflow.docker.image.tagmap}}") Map<String, String> dockerImageTagMap,
            @Value("${workflow.docker.image.pull.secret}") String dockerImagePullSecret, KubernetesService kubernetesService) {
        this.dockerImageTagMap = dockerImageTagMap;
        this.dockerImagePullSecret = dockerImagePullSecret;
        this.kubernetesService = kubernetesService;
    }

    public WorkflowDeployment<V1Job> makeWorkflowDeployment(String workflowName, ActiveWorkflow workflow) {
        logger.debug("Make " + workflowName + " deployment for session " + workflow.getSessionId());
        final String sessionId = workflow.getSessionId();
        final String appName = determineAppName(sessionId, workflowName);
        final String inputs = new String(workflow.getInputs());
        final String runId = workflow.getRunId();

        if(null == this.determineImageTag(workflowName)){
            logger.error("Unsupported workflowName: "+workflowName);
            return null;
        }

        final String datadirPvcName = DATADIRECTORY_PVC_NAME_PREFIX + sessionId;

        Map<String, String> appLabels = new java.util.HashMap<>(Map.of(
                "type", "workflow",
                "app", appName,
                WORKFLOW_SESSION_ID_LABEL, sessionId,
                RUN_ID_LABEL, runId));

        List<V1EnvVar> environment = new ArrayList<V1EnvVar>();
        V1EnvVar sessionIdEnv = new V1EnvVar();
        sessionIdEnv.name("SESSION_ID");
        sessionIdEnv.value(sessionId);
        environment.add(sessionIdEnv);

        String sessionHeader = workflow.getSessionHeader();
        if (sessionHeader != null) {
            V1EnvVar expressIdEnv = new V1EnvVar();
            expressIdEnv.name("EXPRESS_ID");
            expressIdEnv.value(sessionHeader.split(",")[0]);
            environment.add(expressIdEnv);

            V1EnvVar expressKeyEnv = new V1EnvVar();
            expressKeyEnv.name("EXPRESS_KEY");
            expressKeyEnv.value(sessionHeader.split(",")[1]);
            environment.add(expressKeyEnv);

            V1EnvVar tokenEndpointEnv = new V1EnvVar();
            tokenEndpointEnv.name("TOKEN_ENDPOINT");
            tokenEndpointEnv.value(TOKEN_ENDPOINT);
            environment.add(tokenEndpointEnv);

            appLabels.put("expressId", sessionHeader.split(",")[0]);
        }

        logger.debug("Building job");
        V1Job job = buildRunOnceJob(sessionId, workflowName, appName, appLabels, datadirPvcName, environment);
        logger.debug("Building configmap");
        V1ConfigMap inputsConfigMap = buildInputsConfigMap(sessionId, workflowName, appLabels, inputs, runId);

        logger.debug("construct deployment");
        WorkflowDeployment<V1Job> deployment = new WorkflowDeployment<>(runId, inputsConfigMap,
                datadirPvcName, job);
        logger.debug(deployment.toString());
        return deployment;

    }

    public WorkflowDeployment<V1CronJob> makeCronWorkflowDeployment(String workflowName, ActiveWorkflow workflow) {
        logger.debug("Make " + workflowName + " deployment for session " + workflow.getSessionId());
        final String sessionId = workflow.getSessionId();
        final String appName = determineAppName(sessionId, workflowName);
        final String inputs = new String(workflow.getInputs());
        final String runId = workflow.getRunId();

        if(null == this.determineImageTag(workflowName)){
            logger.error("Unsupported workflowName: "+workflowName);
            return null;
        }

        Map<String, String> appLabels = new java.util.HashMap<>(Map.of(
                "type", "workflow",
                "app", appName,
                WORKFLOW_SESSION_ID_LABEL, sessionId,
                RUN_ID_LABEL, runId));

        List<V1EnvVar> environment = new ArrayList<V1EnvVar>();
        V1EnvVar sessionIdEnv = new V1EnvVar();
        sessionIdEnv.name("SESSION_ID");
        sessionIdEnv.value(sessionId);
        environment.add(sessionIdEnv);

        V1CronJob cronJob = buildCronJob(sessionId, workflowName, appName, appLabels, environment);
        logger.debug("Building configmap");
        V1ConfigMap inputsConfigMap = buildInputsConfigMap(sessionId, workflowName, appLabels, inputs, runId);

        logger.debug("construct deployment");
        WorkflowDeployment<V1CronJob> deployment = new WorkflowDeployment<>(runId, inputsConfigMap,
                null, cronJob);
        logger.debug(deployment.toString());
        return deployment;

    }

    public static String determineAppName(String sessionId, String workflowName) {
        return WORKFLOW_PREFIX + "-" + sessionId;
    }

    private String determineImageTag(String workflowName) {
        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream("/home/swirrl/swirrl-workflow.properties"));
            String property = String.format("workflow.%s.docker.image.tag", workflowName);
            String value = properties.getProperty(property);
            if (value == null) {
                // Look for custom workflows.
                Map<String, String> customWorkflows = kubernetesService.readConfigMap(CUSTOM_WORKFLOW_CONFIGMAP_NAME);
                if (null != customWorkflows && customWorkflows.containsKey(workflowName)) {
                    return determineRegistry()+customWorkflows.get(workflowName);
                }
            }
            return value;
        } catch (FileNotFoundException e) {
            return dockerImageTagMap.get(workflowName);
        } catch (IOException e) {
            e.printStackTrace();
            return dockerImageTagMap.get(workflowName);
        }
    }

    private String determineRegistry() throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream("/home/swirrl/swirrl-workflow.properties"));
        // Just get the first tag in the properties list.
        String tag = properties.values().toArray()[0].toString();
        int i = tag.lastIndexOf('/');
        if (i <= 0) return ""; // Local registry when in development on e.g. minikube
        return tag.substring(0, i)+'/';
    }

    private V1Job buildRunOnceJob(String sessionId, String workflowName, String appName, Map<String, String> appLabels,
                           String datadirPvcName, List<V1EnvVar> environment) {
        V1Job job = null;
        String label;
        try {
            String resourceName;
            if (workflowName.startsWith("custom-")) {
                resourceName = "/k8s-specs/workflow/custom/job.yaml";
                label = "custom";
            }
            else {
                resourceName = "/k8s-specs/workflow/" + workflowName + "/job.yaml";
                label = workflowName;
            }
            job = Yaml.loadAs(new InputStreamReader(new ClassPathResource(resourceName).getInputStream()), V1Job.class);
        } catch (final IOException | ConstructorException e) {
            logger.error("Exception occurred reading job.yaml for " + workflowName + " workflow.", e);
            return null;
        }

        final V1LocalObjectReference dockerImagePullSecretObj = new V1LocalObjectReference();
        dockerImagePullSecretObj.setName(dockerImagePullSecret);

        final String jobTimestamp = new SimpleDateFormat("yyyyMMddHHmmSS").format(new Date());

        job = new V1JobBuilder(job)
                // Every job.yaml section has its own line here, except for the volumes.
                .editMetadata().withName(appName + "-" + jobTimestamp).withLabels(appLabels).endMetadata()
                .editSpec().editTemplate().editMetadata().withName(appName).withLabels(appLabels).endMetadata()
                // Image pull secret name.
                .editSpec()
                .withImagePullSecrets(dockerImagePullSecretObj).endSpec()
                // Container and container image tag
                .editSpec().editMatchingContainer(item -> ("cwl-" + label + "-workspace-data").equals(item.getName()))
                .addAllToEnv(environment)
                .withImage(determineImageTag(workflowName))
                .editMatchingVolumeMount(item -> item.getName().equals(WORKFLOW_PREFIX + "-" + label + "-persistent-storage"))
                .withSubPath(sessionId)
                .endVolumeMount()
                .endContainer()
                // Persistent volume claim.
                .editMatchingVolume(item -> (WORKFLOW_PREFIX + "-" + label + "-persistent-storage").equals(item.getName()))
                .editPersistentVolumeClaim().withClaimName(datadirPvcName).endPersistentVolumeClaim()
                .endVolume()
                // Inputs config map.
                .editMatchingVolume(item -> "configmap-inputs".equals(item.getName()))
                .editConfigMap().withName(WORKFLOW_PREFIX + "-" + workflowName + "-inputs-" + sessionId)
                .endConfigMap()
                .endVolume()
                // The end...
                .endSpec().endTemplate().endSpec()
                .build();
        logger.debug(Yaml.dump(job));
        return job;
    }

    private V1CronJob buildCronJob(String sessionId, String workflowName, String appName, Map<String, String> appLabels,
                                        List<V1EnvVar> environment) {
        V1CronJob job = null;
        try {
            job = Yaml.loadAs(new InputStreamReader(
                    new ClassPathResource("/k8s-specs/workflow/" + workflowName + "/job.yaml").getInputStream()), V1CronJob.class);
        } catch (final IOException | ConstructorException e) {
            logger.error("Exception occurred reading job.yaml for " + workflowName + " workflow.", e);
            return null;
        }

        final V1LocalObjectReference dockerImagePullSecretObj = new V1LocalObjectReference();
        dockerImagePullSecretObj.setName(dockerImagePullSecret);

        final String jobTimestamp = new SimpleDateFormat("yyyyMMddHHmmSS").format(new Date());

        job = new V1CronJobBuilder(job)
                // Every job.yaml section has its own line here, except for the volumes.
                // Not appending time stamp to the name, as it will exceed the 52 char
                // lenght limit. Also, maybe it will prevent the user from submitting
                // more than one cronjob. Although, currently, the prefix is always 'cwl-',
                // so only one cronjob in total per session is then possible. Perhaps,
                // the prefix should have the workflowName instead.
                .editMetadata().withName(workflowName+"-"+sessionId).withLabels(appLabels).endMetadata()
                .editSpec().editJobTemplate()
                .withNewMetadata()
                .withName(appName)
                .withLabels(appLabels)
                .endMetadata()
                .editSpec().editTemplate().editSpec()
                // Image pull secret name.
                .withImagePullSecrets(dockerImagePullSecretObj).endSpec()
                // Container and container image tag
                .editSpec().editMatchingContainer(item -> (workflowName).equals(item.getName()))
                .addAllToEnv(environment)
                .withImage(determineImageTag(workflowName))
                .endContainer()
                // Inputs config map.
                .editMatchingVolume(item -> "configmap-inputs".equals(item.getName()))
                .editConfigMap().withName(WORKFLOW_PREFIX + "-" + workflowName + "-inputs-" + sessionId)
                .endConfigMap()
                .endVolume()
                // The end...
                .endSpec().endTemplate().endSpec().endJobTemplate().endSpec()
                .build();
        logger.debug(Yaml.dump(job));
        return job;
    }
    private static V1ConfigMap buildInputsConfigMap(String notebookId, String workflowName, Map<String, String> appLabels, String inputs, String runId) {
        V1ConfigMap inputsConfigMap = new V1ConfigMapBuilder()
                .withNewMetadata().withName(WORKFLOW_PREFIX + "-" + workflowName + "-inputs-" + notebookId).withLabels(appLabels).endMetadata()
                .withData(Map.of("inputs.yml", inputs))
                .addToData(Map.of("metadata.txt", runId))
                .build();
        logger.debug(Yaml.dump(inputsConfigMap));

        return inputsConfigMap;
    }

}
