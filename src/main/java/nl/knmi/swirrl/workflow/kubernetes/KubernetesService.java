package nl.knmi.swirrl.workflow.kubernetes;

import io.kubernetes.client.PodLogs;
import io.kubernetes.client.custom.V1Patch;
import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.Configuration;
import io.kubernetes.client.openapi.apis.BatchV1Api;
import io.kubernetes.client.openapi.apis.CoreV1Api;
import io.kubernetes.client.openapi.models.*;
import io.kubernetes.client.util.Config;
import nl.knmi.swirrl.workflow.SwirrlApiConfiguration;
import org.apache.http.HttpStatus;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import nl.knmi.swirrl.workflow.model.Job;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Map;

import static java.lang.Boolean.FALSE;
import static nl.knmi.swirrl.workflow.kubernetes.WorkflowDeploymentFactory.SESSION_ID_LABEL;

@Component
@EnableConfigurationProperties(value = SwirrlApiConfiguration.class)
@Scope(value="prototype")
public class KubernetesService {

    private final static Logger logger = LoggerFactory.getLogger(KubernetesService.class);

    private final String NAMESPACE;

    private final String PRETTY = "";
    private final V1DeleteOptions EMPTY_V1_DELETE_OPTIONS = new V1DeleteOptions();
    private final Integer GRACE_PERIOD_SECONDS = 15;
    private final String PROPAGATION_POLICY = "Foreground";

    private final BatchV1Api batchV1Api;
    private final CoreV1Api coreV1Api;

    @Autowired
    public KubernetesService(SwirrlApiConfiguration swirrlApiConfiguration) {
        ApiClient client = null;
        try {
            client = Config.defaultClient();
        } catch (IOException e) {
            throw new RuntimeException("Unable to initialize kubernetes client.");
        }
        // TODO: Determine a correct timeout here!
        client.setConnectTimeout(100);
        Configuration.setDefaultApiClient(client);

        this.coreV1Api = new CoreV1Api();
        this.batchV1Api = new BatchV1Api();
        NAMESPACE = swirrlApiConfiguration.getNamespace();
        assert NAMESPACE != null;
    }

    /**
     * creates a Job from a deployment.
     * @param deployment
     * @return the uuid of the created job. It should be possible to use this
     * UUID to obtain the job's status with the following code:
     * try { this.coreV1Api.listNamespacedPod(NAMESPACE, PRETTY, "continue", "controller-uid", true, NAMESPACE, 1, NAMESPACE, 60, false);	} catch (final ApiException e) {}
     */
    public Job createJob(WorkflowDeployment<V1Job> deployment) {
        V1Job job = deployment.getJob();
        if (null == job) {
            logger.warn("Received null job.");
        }

        Job apiResponse = new Job();
        try {
            V1Job result = batchV1Api.createNamespacedJob(NAMESPACE, job, PRETTY, null, null);
            apiResponse.setId(deployment.getRunId());
            apiResponse.setLogs(new byte[0]);
            apiResponse.setStartTime(DateTime.now().toString());
            apiResponse.setStatus("");
            apiResponse.setImageID(job.getSpec().getTemplate().getSpec().getContainers().get(0).getImage());
        } catch (final ApiException e) {
            logger.error("Impossible to create job due to exception: "+e.getCode()+"\n"+e);
            e.printStackTrace();
        }

        return apiResponse;
    }

    public Job createCronJob(WorkflowDeployment<V1CronJob> deployment) {
        V1CronJob job = deployment.getJob();
        if (null == job) {
            logger.warn("Received null job.");
        }

        Job apiResponse = new Job();
        try {
            V1CronJob result = batchV1Api.createNamespacedCronJob(NAMESPACE, job, PRETTY, null, null);
            apiResponse.setId(deployment.getRunId());
            apiResponse.setLogs(new byte[0]);
            apiResponse.setStartTime(DateTime.now().toString());
            apiResponse.setStatus("");
            apiResponse.setImageID(job.getSpec().getJobTemplate().getSpec().getTemplate().getSpec().getContainers().get(0).getImage());
        } catch (final ApiException e) {
            if (e.getCode() == HttpStatus.SC_CONFLICT)
                logger.info("Cronjob "+deployment.getRunId()+" already exists.");
            else {
                logger.error("Impossible to create cronjob due to exception: " + e.getCode() + "\n" + e);
                e.printStackTrace();
            }
            return null;
        }

        return apiResponse;
    }

    public void deleteCronJobByLabel(String workflowName, String sessionId) {
        String label = workflowName+"-"+sessionId;
        try {
            logger.debug("Deleting cron job "+label);
            batchV1Api.deleteNamespacedCronJob(label, NAMESPACE, PRETTY, null, GRACE_PERIOD_SECONDS, Boolean.TRUE, PROPAGATION_POLICY, EMPTY_V1_DELETE_OPTIONS);
        } catch (final ApiException e) {
            logger.error("Could not delete cron job "+label+" due to exception: "+e.getCode()+"\n"+e);
        }
    }

    /**
     * creates a Job from a deployment.
     * @param deployment
     * @param jobId
     * @return the uuid of the created job. It should be possible to use this
     * UUID to obtain the job's status with the following code:
     * try { this.coreV1Api.listNamespacedPod(NAMESPACE, PRETTY, "continue", "controller-uid", true, NAMESPACE, 1, NAMESPACE, 60, false);	} catch (final ApiException e) {}
     */
    public Job createDeleteJob(DeleteStageJob deployment, String jobId) {
        V1Job job = deployment.getJob();
        if (null == job) {
            logger.warn("Received null job.");
        }

        Job apiResponse = new Job();
        try {
            V1Job result = batchV1Api.createNamespacedJob(NAMESPACE, job, PRETTY, null, null);
            apiResponse.setId(jobId);
            apiResponse.setLogs(new byte[0]);
            apiResponse.setStartTime(DateTime.now().toString());
            apiResponse.setStatus("");
        } catch (final ApiException e) {
            logger.error("Impossible to create job due to exception: "+e.getCode()+"\n"+e);
            e.printStackTrace();
        }
        return apiResponse;
    }

    public Job getJobStatus(String label, String jobId) {

        V1Pod pod = this.retrieveLatestJobPod(label, jobId);
        if (null == pod) return null;

        String status = pod.getStatus().getPhase();
        String imageID = null;
        if (pod.getStatus().getContainerStatuses() != null)
            imageID = pod.getStatus().getContainerStatuses().get(0).getImageID();
        logger.debug("Job's pod UID: "+pod.getMetadata().getUid()+"\nStatus: "+pod.getStatus().getPhase());
        logger.debug("Job's container status: "+pod.getStatus().getContainerStatuses());
        byte[] logs = getPodLogs(pod);

        Job job = new Job();
        job.setStatus(status);
        job.setImageID(imageID);
        job.setId(jobId);
        job.setLogs(logs);
        return job;
    }

    public byte[] getPodLogs(V1Pod pod) {
        PodLogs logs = new PodLogs();
        String status = pod.getStatus().getPhase();
        switch (status) {
            case "Succeeded" : case "Failed" :
                try {
                    return logs.streamNamespacedPodLog(pod).readAllBytes();
                } catch (final ApiException e) {
                    logger.warn("Cannot read logs from pod " + pod.getMetadata().getName() + "\nException:\n" + e);
                } catch (final IOException e) {
                    logger.warn("Cannot read logs from pod " + pod.getMetadata().getName() + "\nException:\n" + e);
                }
                return "Error retrieving logs.".getBytes();
            case "Pending":
                return "No logs available while job is pending.".getBytes();
            case "Running":
                return "No logs available while job is running.".getBytes();
            default:
                return "No logs available.".getBytes();
        }
    }

    /*
     * Retrieves the most recent pod associated with the job identified by runId.
     * @param jobId
     * @return A Pod.
     */
    public V1Pod retrieveLatestJobPod(String label, String jobId) {
        V1PodList podList = this.retrievePodsByLabel(label+"="+jobId);
        if (null == podList) {
            logger.error("No pods were found for "+label+"="+jobId);
            return null;
        }
        V1Pod jobPod = null;
        OffsetDateTime mostRecentPodCreationTime = OffsetDateTime.of(1900, 1, 1, 0, 0, 0, 0, ZoneOffset.of("+00:00"));
        for (V1Pod pod : podList.getItems()) {
            OffsetDateTime creationTimestamp = pod.getMetadata().getCreationTimestamp();
            if (creationTimestamp.compareTo(mostRecentPodCreationTime) > 0) {
                jobPod = pod;
                logger.debug("Pod: "+pod.getMetadata().toString());
                mostRecentPodCreationTime = creationTimestamp;
            }
        }
        if (null != jobPod) return jobPod;
        else return null;
    }

    public void killJob(String label, String jobId) {
        try {
            V1JobList jobs = batchV1Api.listNamespacedJob(NAMESPACE, PRETTY, false, null, null, label,
                    null, null, null, null, false);
            for (V1Job job : jobs.getItems()) {
                String jobName = job.getMetadata().getName();
                try {
                    V1Status status = batchV1Api.deleteNamespacedJob(jobName, NAMESPACE, PRETTY,
                            null, 1, null, "Foreground", null);
                    if (status.getCode() != HttpStatus.SC_OK) {
                        logger.error("Could not delete job " + jobName + ". Reason: " + status.getReason());
                        continue;
                    }
                    logger.info("Job " + jobName + " deleted.");
                } catch (ApiException e2) {
                    logger.error("Deleting job " + jobName + " threw exception: " + e2.getResponseBody());
                }
            }
        } catch (ApiException e1) {
            logger.error("Could not find jobs with id " + jobId + " threw exception: " + e1.getResponseBody());
        } catch (Exception e3) {
            logger.error("Exception while trying to delete job "+jobId+". "+e3.getMessage()+"\n"+e3.getStackTrace());
        }
    }

    /**
     * Retrieves a list of pods having the specified label.
     * Label should be written as:
     * <pre>
     *     label-name=label-value
     * </pre>
     *
     * For example:
     * <pre>
     *     controller-uid=1234567890
     * </pre>
     * @param label
     * @return A ResponseEntity which contains an V1PodList when HttpStatus is OK and the list is not empty,
     * otherwise an Error
     */
    public V1PodList retrievePodsByLabel(String label) {
        V1PodList v1PodList = new V1PodList();

        try {
            v1PodList = coreV1Api.listNamespacedPod(NAMESPACE,
                    PRETTY,
                    FALSE,
                    null,
                    null,
                    label,
                    null,
                    null,
                    null,
                    null,
                    false);
        } catch (ApiException e) {
            logger.error("Impossible to retrieve pod list with label "+label+" due to exception.", e.getCode(), e);
        }

        return v1PodList;
    }

    /**
     * Retrieves a list of PVCs having the specified label.
     * Label should be written as:
     * <pre>
     *     label-name=label-value
     * </pre>
     *
     * For example:
     * <pre>
     *     controller-uid=1234567890
     * </pre>
     * @param label
     * @return A ResponseEntity which contains an V1PersistentVolumeClaimList when HttpStatus is OK and the list is not empty,
     * otherwise an Error
     */
    public V1PersistentVolumeClaimList retrievePVCsByLabel(String label) {
        V1PersistentVolumeClaimList v1PVCList = null;
        assert NAMESPACE != null;
        logger.debug("NAMESPACE:"+ NAMESPACE);
        try {
            v1PVCList = coreV1Api.listNamespacedPersistentVolumeClaim(NAMESPACE,
                    PRETTY,
                    FALSE,
                    null,
                    null,
                    label,
                    null,
                    null,
                    null,
                    null,
                    false);
        } catch (ApiException e) {
            logger.error("Impossible to retrieve PVC list due to exception: "+e.getCode() + "\n" + e);
        }

        if (null != v1PVCList && v1PVCList.getItems().isEmpty()) {
            logger.error(String.format("No PVCs were found for label %s", label));
        }
        return v1PVCList;
    }

    public int createOrReplaceConfigMap(V1ConfigMap inputsConfigMap) {
        if (null == inputsConfigMap) {
            logger.error( "Input configmap equals null.");
            return HttpURLConnection.HTTP_INTERNAL_ERROR;
        }

        V1ConfigMap inputsConfigMapResult = null;
        try {
            inputsConfigMapResult = coreV1Api.replaceNamespacedConfigMap(
                    inputsConfigMap.getMetadata().getName(), NAMESPACE, inputsConfigMap, PRETTY, null, null);
        } catch (final ApiException e) {

            // Not found is an expected response when the config map does not exist yet.
            if (HttpURLConnection.HTTP_NOT_FOUND != e.getCode()) {
                logger.error("Impossible to replace configmap due to exception. Reason: "+e.getCode());
                return e.getCode();
            }
        }
        if (inputsConfigMapResult == null) {
            try {
                inputsConfigMapResult = coreV1Api.createNamespacedConfigMap(NAMESPACE, inputsConfigMap, PRETTY, null, null);
            } catch (final ApiException e) {
                logger.error("Impossible to create configmap after retry due to exception. Reason: "+e.getCode());
                e.printStackTrace();
                return e.getCode();
            }
        }
        return HttpURLConnection.HTTP_OK;
    }

    public Map<String, String> readConfigMap(String configmapName) {
        if (configmapName == null) {
            logger.error("Cannot read configmap with null name");
            return null;
        }
        try {
            V1ConfigMap configMap = coreV1Api.readNamespacedConfigMap(configmapName, NAMESPACE, PRETTY);
            return configMap.getData();
        } catch (final ApiException e) {
            logger.error("Could not read configmap <"+configmapName+">. Reason:"+e.getCode()+" "+e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    /*
     * Patch a Persistent Volume Claim spec.
     * @param name name of the PVC
     * @param patch String containing a jsonpatch (https://jsonpatch.com)
     */
    public void patchNamedPvcBySession(String sessionid, String name, String patch) {
        V1PersistentVolumeClaimList pvcs = retrievePVCsByLabel(SESSION_ID_LABEL + "=" + sessionid);

        for (V1PersistentVolumeClaim pvc : pvcs.getItems()) {
            // Construct a jsonpatch: https://jsonpatch.com
            if (!name.equals(pvc.getMetadata().getName())) return;
            try {
                V1PersistentVolumeClaim patched = coreV1Api.patchNamespacedPersistentVolumeClaim(
                        name, NAMESPACE, new V1Patch(patch), PRETTY,null, null, null);
                logger.trace(patched.toString());
            } catch (ApiException e) {
                logger.error("Exception patching pvc "+ name);
                logger.error(e.getResponseBody());
            }
        }
    }
}
