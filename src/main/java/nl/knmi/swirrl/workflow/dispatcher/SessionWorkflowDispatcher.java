package nl.knmi.swirrl.workflow.dispatcher;

import io.kubernetes.client.openapi.models.V1CronJob;
import io.kubernetes.client.openapi.models.V1Job;
import io.kubernetes.client.openapi.models.V1PersistentVolumeClaimList;
import nl.knmi.swirrl.workflow.SwirrlApiConfiguration;
import nl.knmi.swirrl.workflow.kubernetes.KubernetesService;
import nl.knmi.swirrl.workflow.kubernetes.WorkflowDeployment;
import nl.knmi.swirrl.workflow.kubernetes.WorkflowDeploymentFactory;
import nl.knmi.swirrl.workflow.model.Job;
import nl.knmi.swirrl.workflow.provenance.WorkflowProvenanceService;
import nl.knmi.swirrl.workflow.queue.WorkflowQueue;
import org.apache.hc.core5.http.HttpResponse;
import org.apache.hc.core5.http.HttpStatus;
import org.joda.time.DateTime;
import org.openapitools.v1_0.model.ActiveWorkflow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.HttpURLConnection;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

public class SessionWorkflowDispatcher extends Thread {

    private static final Logger logger = LoggerFactory.getLogger(SessionWorkflowDispatcher.class);

    private final KubernetesService kubernetesService;

    private final String sessionId;
    private final Queue<ActiveWorkflow> queue;
    private final Queue<ActiveWorkflow> rollbackQueue;
    private final WorkflowDeploymentFactory workflowDeploymentFactory;
    private final WorkflowProvenanceService provenanceService;
    private final WorkflowQueue wfQueue;

    private final String JOB_STATUS_TIMEOUT = "Time Out";
    private final String JOB_STATUS_FAILED = "Failed";
    private final String JOB_STATUS_SUCCEEDED = "Succeeded";
    private final String JOB_STATUS_UNKNOWN = "Unknown";

    public SessionWorkflowDispatcher(String sessionId, Queue<ActiveWorkflow> queue, Queue<ActiveWorkflow> rollbackQueue,
                                     WorkflowDeploymentFactory workflowDeploymentFactory,
                                     WorkflowProvenanceService provenanceService,
                                     WorkflowQueue wfQueue, SwirrlApiConfiguration swirrlApiConfiguration) {
        super(sessionId);
        this.sessionId = sessionId;
        this.queue = queue;
        this.rollbackQueue = rollbackQueue;
        this.workflowDeploymentFactory = workflowDeploymentFactory;
        this.provenanceService = provenanceService;
        this.wfQueue = wfQueue;
        this.kubernetesService = new KubernetesService(swirrlApiConfiguration);
    }

    /*
     * Poll the queue. If rollbacks are scheduled pause the execution. 
     * If a normal workflow message arrives start a workflow.
     * If a "delete" message arrives, delete the queue from the map through
     * a method provided by WorkflowQueue. Then end the thread.
     */
    @Override
    public void run() {
        int emptyCount = 0;
        while (true) {
            if (!rollbackQueue.isEmpty()) {
                try { Thread.sleep(5*1000); } catch (Exception e) { ; }
                continue;
            }
            ActiveWorkflow workflow = (ActiveWorkflow)queue.poll();
            if (null != workflow) {
                emptyCount = 0;
                if (workflow.getActivity().equals(ActiveWorkflow.DELETE_SESSION_ACTIVITY_COMMAND)) {
                    wfQueue.deleteSessionQueueFromWorkflowQueues(workflow.getSessionId());
                    logger.debug("Ending workflow dispatcher for session "+workflow.getSessionId());
                    return;
                }
                if (workflow.getActivity().equals(ActiveWorkflow.DELETE_CRON_WORKFLOW_COMMAND)) {
                    logger.debug("Deleting cron workflow "+workflow.getWorkflowName()+" of session "+workflow.getSessionId());
                    kubernetesService.deleteCronJobByLabel(workflow.getWorkflowName(), workflow.getSessionId());
                    continue;
                }
                logger.debug("Going to start workflow on session: " + workflow.getSessionId());
                try {
                    runWorkflow(workflow.getWorkflowName(), workflow);
                } catch (Exception e) {
                    logger.error("Exception occurred while running workflow name "+workflow.getRunId()+
                            ", on session "+workflow.getSessionId()+". Exception: "+e.getMessage()+e.getStackTrace());
                }
            } else {
                logger.trace("Nothing in queue for session " + this.sessionId);
                if (emptyCount > 3600) {
                    logger.info("Ending dispatcher for session {} due to no activity for 1 hour.", this.sessionId);
                    // workflow is null, here so we need to use session id from our own members.
                    wfQueue.deleteSessionQueueFromWorkflowQueues(this.sessionId);
                    return;
                }
                emptyCount++;
            }
            try { Thread.sleep(1*1000); } catch (Exception e) { ; }
        }
    }

    public void runWorkflow(String workflowName, ActiveWorkflow workflow) {
        String SessionId = workflow.getSessionId();
        String runId = workflow.getRunId();

        V1PersistentVolumeClaimList pvcList = kubernetesService.retrievePVCsByLabel(
                WorkflowDeploymentFactory.SESSION_ID_LABEL + "=" + SessionId);

        // Make sure we have a valid session id before running the workflow:
        if (null == pvcList || pvcList.getItems().size() == 0) {
            logger.error("Workflow requires a valid session ID but id \'" + SessionId + "\' does not exist.");
            return;
        }

        Job job = null;
        if (workflowName.contains("cron")) {
            job = dispatchCronJob(workflowName, workflow);
            if (null != job) logger.info("Submitted cronjob " + workflowName + ": " + job.getId());
            return;
        } else {
            job = dispatchRunOnceJob(workflowName, workflow);
        }
        if (null == job) {
            logger.error("Could not start job "+runId);
            return;
        }

        String expandProvenanceResponse = null;
        waitForWorkflow(job);
        if (job.getStatus().compareTo(JOB_STATUS_TIMEOUT) == 0 ||
                job.getStatus().compareTo(JOB_STATUS_FAILED) == 0 ||
                job.getStatus().compareTo(JOB_STATUS_UNKNOWN) == 0) {
            logger.error("Job " + runId + " timed out before it was finished or failed.");
            String provenanceBindings = provenanceService.traceFailRunWorkflow(workflow, job);
            kubernetesService.killJob(WorkflowDeploymentFactory.RUN_ID_LABEL, job.getId());
            expandProvenanceResponse = provenanceService.expandProvTemplate(provenanceBindings, "fail_workflow");
            logger.debug("Expand bindings response:\n" + expandProvenanceResponse);
        } else if (job.getStatus().compareTo(JOB_STATUS_SUCCEEDED) == 0) {
            // Create and store the workflow's provenance.
            String provenanceBindings = provenanceService.traceProvenance(workflow, job);
            expandProvenanceResponse = provenanceService.expandProvTemplate(provenanceBindings, "run_workflow");
            logger.debug("Expand bindings response:\n" + expandProvenanceResponse);
        }

        if (null != expandProvenanceResponse) {
            logger.debug("Storing workflow provenance");
            HttpResponse storeProvenanceResponse = provenanceService.saveProvenanceSession(expandProvenanceResponse);
            if (HttpStatus.SC_OK != storeProvenanceResponse.getCode()) {
                logger.error("Failed to store provenance", storeProvenanceResponse.getReasonPhrase());
            }
        }
        String provDoc = provenanceService.extractProvenanceFromLogs(workflow, job);

        if (null != provDoc) {
            logger.debug("Storing ROOKWPS provenance from logs");
            HttpResponse storeProvenanceResponse = provenanceService.saveProvenanceSession(provDoc);
            if (HttpStatus.SC_OK != storeProvenanceResponse.getCode()) {
                logger.error("Failed to store provenance", storeProvenanceResponse.getReasonPhrase());
            }
        }
    }

    private Job dispatchRunOnceJob(String workflowName, ActiveWorkflow workflow) {
        WorkflowDeployment<V1Job> workflowDeployment = workflowDeploymentFactory.makeWorkflowDeployment(workflowName, workflow);
        if (null == workflowDeployment) {
            logger.warn("Could not create workflow deployment for "+workflowName+
                    " job with session ID "+workflow.getSessionId()+
                    " and job ID "+workflow.getRunId());
            return null;
        }
        logger.debug( workflowName+" deployment created");

        logger.debug("Create inputs config map for "+workflowName+" job");
        if (HttpURLConnection.HTTP_OK != kubernetesService.createOrReplaceConfigMap(workflowDeployment.getInputsConfigMap())) {
            logger.error("Could not create of replace inputs configmap for session "+sessionId);
            return null;
        }
        logger.debug("Starting job "+workflow.getRunId());
        return kubernetesService.createJob(workflowDeployment);
    }

    private Job dispatchCronJob(String workflowName, ActiveWorkflow workflow) {
        WorkflowDeployment<V1CronJob> workflowDeployment = workflowDeploymentFactory.makeCronWorkflowDeployment(workflowName, workflow);
        if (null == workflowDeployment) {
            logger.warn("Could not create workflow deployment for "+workflowName+
                    " job with session ID "+workflow.getSessionId()+
                    " and job ID "+workflow.getRunId());
            return null;
        }
        logger.debug( workflowName+" deployment created");

        logger.debug("Create inputs config map for "+workflowName+" job");
        if (HttpURLConnection.HTTP_OK != kubernetesService.createOrReplaceConfigMap(workflowDeployment.getInputsConfigMap())) {
            logger.error("Could not create of replace inputs configmap for session "+sessionId);
            return null;
        }
        logger.debug("Submitting cronjob "+workflow.getRunId());
        return kubernetesService.createCronJob(workflowDeployment);
    }

    private Boolean waitForWorkflow(Job job) {
        Boolean jobFinished = false;
        int timeout = 180; // half an hour (sleep 10s * 180 = 1800)
        int sleeptime = 10;
        try {
            logger.debug("Going to wait "+timeout*sleeptime+" seconds for job " + job.getId() + " to end.");
            while (timeout > 0 && !jobFinished) {
                TimeUnit.SECONDS.sleep(sleeptime);
                Job j = kubernetesService.getJobStatus(WorkflowDeploymentFactory.RUN_ID_LABEL, job.getId());
                job.setEndTime(DateTime.now().toString());
                if (null == j) {
                    logger.warn("Could not get status of job: "+job.getId());
                    job.setStatus(JOB_STATUS_UNKNOWN);
                    job.setImageID(job.getImageID()); // Best guess from the created spec
                    break;
                }
                String status = j.getStatus();
                logger.debug("Job status: "+status);
                job.setImageID(j.getImageID());
                job.setLogs(j.getLogs());
                job.setStatus(j.getStatus());
                String JOB_STATUS_PENDING = "Pending";
                if (status.compareTo(JOB_STATUS_SUCCEEDED) == 0 || status.compareTo(JOB_STATUS_FAILED) == 0) {
                    jobFinished = true;
                } else if (status.compareTo(JOB_STATUS_PENDING) != 0) {
                    // Only decrease timer when job no longer in status pending
                    timeout--;
                    logger.debug("Status = "+status+", time left: "+timeout*sleeptime+" seconds.");
                }
            }
            logger.debug("Job " + job.getId() + " ended.");
        } catch (InterruptedException exception) {
            // TODO: Handle this exception neatly.
            logger.error("Exception: ", exception);
            job.setStatus(JOB_STATUS_UNKNOWN);
        }
        if (!jobFinished) job.setStatus(JOB_STATUS_TIMEOUT);

        return jobFinished;
    }

}
