package nl.knmi.swirrl.workflow.dispatcher;

import nl.knmi.swirrl.workflow.SwirrlApiConfiguration;
import nl.knmi.swirrl.workflow.kubernetes.DeleteStageJob;
import nl.knmi.swirrl.workflow.kubernetes.DeleteStageJobFactory;
import nl.knmi.swirrl.workflow.kubernetes.KubernetesService;
import nl.knmi.swirrl.workflow.model.Job;
import nl.knmi.swirrl.workflow.provenance.WorkflowProvenanceService;
import nl.knmi.swirrl.workflow.queue.WorkflowQueue;
import org.apache.hc.core5.http.HttpResponse;
import org.apache.hc.core5.http.HttpStatus;
import org.joda.time.DateTime;
import org.openapitools.v1_0.model.ActiveWorkflow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class RollbackDispatcher extends Thread {

    private static final Logger logger = LoggerFactory.getLogger(RollbackDispatcher.class);

    private KubernetesService kubernetesService;

    private final String sessionId;
    private Queue queue;
    private DeleteStageJobFactory deleteStageJobFactory;
    private WorkflowProvenanceService provenanceService;
    private WorkflowQueue wfQueue;
    private SwirrlApiConfiguration swirrlApiConfiguration;

    public RollbackDispatcher(String sessionId, Queue queue,
                                     DeleteStageJobFactory deleteStageJobFactory,
                                     WorkflowProvenanceService provenanceService,
                                     WorkflowQueue wfQueue, SwirrlApiConfiguration swirrlApiConfiguration) {
        super(sessionId);
        this.sessionId = sessionId;
        this.queue = queue;
        this.deleteStageJobFactory = deleteStageJobFactory;
        this.provenanceService = provenanceService;
        this.wfQueue = wfQueue;
        this.swirrlApiConfiguration = swirrlApiConfiguration;
        this.kubernetesService = new KubernetesService(swirrlApiConfiguration);
    }

    /*
     * Poll the queue. If a normal workflow message arrives start a workflow.
     * If a "delete" message arrives, delete the queue from the map through
     * a method provided by WorkflowQueue. Then end the thread.
     */
    @Override
    public void run() {
        while (true) {
            ActiveWorkflow workflow = (ActiveWorkflow)queue.poll();
            if (null != workflow) {
                if (workflow.getActivity().equals(ActiveWorkflow.DELETE_SESSION_ACTIVITY_COMMAND)) {
                    wfQueue.deleteSessionQueueFromWorkflowQueues(workflow.getSessionId());
                    logger.debug("Ending workflow dispatcher for session "+workflow.getSessionId());
                    return;
                }
                logger.debug("Going to start rollback on session: " + workflow.getSessionId());
                try {
                    rollbackWorkflow(workflow);
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.error("Exception occurred while running workflow name "+workflow.getWorkflowName()+
                            ", on session "+workflow.getSessionId()+". Exception: "+e.getMessage()+e.getStackTrace());
                    throw  e;
                }
            } else logger.trace("Rollback - Nothing in queue for session "+sessionId);
            try { Thread.sleep(1*1000); } catch (Exception e) { ; }
        }
    }

    public void rollbackWorkflow(ActiveWorkflow workflow) {
        String jobId = UUID.randomUUID().toString();
        logger.info("Job {} rolling back workflow for session {}", jobId, workflow.getSessionId());

        DeleteStageJob rollbackDeployment = deleteStageJobFactory.makeRollbackDeployment(workflow.getSessionId(),
                provenanceService.getLatestRunIds(provenanceService.getLatestActivityIds(workflow.getSessionId())), jobId);

        try {
            kubernetesService.createOrReplaceConfigMap(rollbackDeployment.getRollbackConfigMap());
        } catch (Exception e) {
            logger.error(String.valueOf(e.getStackTrace()));
        }

        Job deleteJob = kubernetesService.createDeleteJob(rollbackDeployment, jobId);
        if (null == deleteJob) {
            logger.error("Could not start delete job.");
            return;
        }
       
        if (!waitForWorkflow(deleteJob)) logger.error("Job " + jobId + " timed out before it was finished.");
        String provenanceBindings = provenanceService.traceProvenanceDeleteLatestStage(workflow, deleteJob);
        String expandProvenanceResponse = provenanceService.expandProvTemplate(provenanceBindings, "rollback_workflow");
        logger.debug("Expand bindings response:\n" + expandProvenanceResponse);

        if (null != expandProvenanceResponse) {
            logger.debug("Storing provenance.");
            HttpResponse storeProvenanceResponse = provenanceService.saveProvenanceSession(expandProvenanceResponse);
            if (HttpStatus.SC_OK != storeProvenanceResponse.getCode()) {
                logger.error("Failed to store provenance", storeProvenanceResponse.getReasonPhrase());
            }
        }
    }


    private Boolean waitForWorkflow(Job job) {
        Boolean jobFinished = false;
        int timeout = 180; // half an hour (sleep 10s * 180 = 1800)
        try {
            logger.debug("Going to wait for job " + job.getId() + " to end.");
            while (timeout > 0 && !jobFinished) {
                TimeUnit.SECONDS.sleep(10);
                Job j = kubernetesService.getJobStatus(DeleteStageJobFactory.JOB_ID_LABEL, job.getId());
                if (null == j) {
                    logger.warn("Could not get status of job: "+job.getId());
                    break;
                }
                String status = j.getStatus();
                if (status.compareTo("Succeeded") == 0 || status.compareTo("Failed") == 0) {
                    job.setEndTime(DateTime.now().toString());
                    job.setLogs(j.getLogs());
                    jobFinished = true;
                }
                timeout--;
            }
            logger.debug("Job " + job.getId() + " ended.");
        } catch (InterruptedException exception) {
            logger.error("Exception: ", exception);
        }

        return jobFinished;
    }

}
