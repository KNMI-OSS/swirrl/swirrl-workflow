package nl.knmi.swirrl.workflow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@SpringBootApplication
public class SwirrlWorkflowApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwirrlWorkflowApplication.class, args);
	}

}
