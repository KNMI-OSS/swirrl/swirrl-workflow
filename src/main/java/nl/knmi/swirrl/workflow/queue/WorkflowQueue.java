package nl.knmi.swirrl.workflow.queue;

import nl.knmi.swirrl.workflow.SwirrlApiConfiguration;
import nl.knmi.swirrl.workflow.dispatcher.RollbackDispatcher;
import nl.knmi.swirrl.workflow.dispatcher.SessionWorkflowDispatcher;
import nl.knmi.swirrl.workflow.kubernetes.DeleteStageJobFactory;
import nl.knmi.swirrl.workflow.kubernetes.WorkflowDeploymentFactory;
import nl.knmi.swirrl.workflow.provenance.WorkflowProvenanceService;
import org.openapitools.v1_0.model.ActiveWorkflow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

@Service
@EnableConfigurationProperties(value = SwirrlApiConfiguration.class)
public class WorkflowQueue {
    private static final Logger logger = LoggerFactory.getLogger(WorkflowQueue.class);

    private static Map<String, Queue> workflowQueues = new HashMap<>();
    private static Map<String, Queue> rollbackQueues = new HashMap<>();

    private WorkflowDeploymentFactory workflowDeploymentFactory;
    private DeleteStageJobFactory deleteStageJobFactory;
    private WorkflowProvenanceService provenanceService;
    private SwirrlApiConfiguration swirrlApiConfiguration;

    @Autowired
    public WorkflowQueue(WorkflowProvenanceService provenanceService, WorkflowDeploymentFactory workflowDeploymentFactory,
                         DeleteStageJobFactory deleteStageJobFactory, SwirrlApiConfiguration swirrlApiConfiguration) {
        this.workflowDeploymentFactory = workflowDeploymentFactory;
        this.deleteStageJobFactory = deleteStageJobFactory;
        this.provenanceService = provenanceService;
        this.swirrlApiConfiguration = swirrlApiConfiguration;
    }

    @KafkaListener(topics="workflow")
    public void newWorkflowHandler(ActiveWorkflow workflow, Acknowledgment acknowledgment) {
        if (workflow == null || workflow.getSessionId() == null) {
            logger.error("Received null message on workflow topic.");
            acknowledgment.acknowledge();
            return;
        }

        logger.debug("New workflow for session: "+workflow.getSessionId());
        if (workflowQueues.size() > 250) {
            // Ideally we want to clean up an idle queue, and continue creating the new one.
            // For that we need to figure out which of the queues has been idle and then
            // preferably remove the one that has been idle the longest.
            logger.warn("Workflow queue size limit (250) reached. Not starting new dispatcher for session {}.",
                    workflow.getSessionId());
            return;
        }

        String sessionId = workflow.getSessionId();
        if (!workflowQueues.containsKey(sessionId)) {

            ConcurrentLinkedQueue<ActiveWorkflow> rollbackQueue = new ConcurrentLinkedQueue<ActiveWorkflow>();
            rollbackQueues.put(sessionId, rollbackQueue);
            RollbackDispatcher rollbackDispatcher = new RollbackDispatcher(sessionId, rollbackQueue,
            deleteStageJobFactory, provenanceService, this, swirrlApiConfiguration);

            ConcurrentLinkedQueue<ActiveWorkflow> wfQueue = new ConcurrentLinkedQueue<ActiveWorkflow>();
            workflowQueues.put(sessionId, wfQueue);
            SessionWorkflowDispatcher workflowDispatcher = new SessionWorkflowDispatcher(sessionId, wfQueue, rollbackQueue,
                    workflowDeploymentFactory, provenanceService, this, swirrlApiConfiguration);

            workflowDispatcher.start();
            rollbackDispatcher.start();
        }
        addWorkflowToSessionQueue(workflow);
        acknowledgment.acknowledge();
    }

    public void deleteSessionQueueFromWorkflowQueues(String sessionId) {
        workflowQueues.remove(sessionId);
    }

    private void addWorkflowToSessionQueue(ActiveWorkflow workflow) {
        if (null != workflow.getWorkflowName() && workflow.getWorkflowName().equals("DELETE_LATEST")){
            Queue rollbackQueue = rollbackQueues.get(workflow.getSessionId());
            rollbackQueue.add(workflow);
        } else {
            Queue wfQueue = workflowQueues.get(workflow.getSessionId());
            wfQueue.add(workflow);
        }
    }
}
