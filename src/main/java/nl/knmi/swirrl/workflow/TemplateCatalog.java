package nl.knmi.swirrl.workflow;

import nl.knmi.swirrl.workflow.provenance.ProvenanceConfiguration;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

@Service
public class TemplateCatalog {
    private Map<String,String> provTemplates;

    @Autowired
    ProvenanceConfiguration provenanceConfiguration;
    private Logger logger = LoggerFactory.getLogger(TemplateCatalog.class);

    public TemplateCatalog(ProvenanceConfiguration provenanceConfiguration) {
        this.provenanceConfiguration = provenanceConfiguration;
        this.provTemplates = new HashMap<String,String>();
    }

    public String getIdByTitle(String title) {
        JSONArray templateList = this.getTemplateList();
        if (null != templateList) {
            try {
                for (int i = 0; i < templateList.length(); i++) {
                    String key_title = templateList.getJSONObject(i).getString("title");
                    String value_id = templateList.getJSONObject(i).getString("id");
                    logger.debug("Template " + i + " : " + key_title + " - " + value_id);
                    if (null != value_id) {
                        this.provTemplates.put(key_title, value_id);
                        if (key_title.equals(title))
                            return value_id;
                    }
                }
            } catch (Exception ex) {
                logger.error("Exception parsing template list: "+ex.getMessage());
            }
        }
        if (this.provTemplates.containsKey(title))
            return this.provTemplates.get(title);
        return null;
    }

    private JSONArray getTemplateList() {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        try {
            HttpPost request = new HttpPost(provenanceConfiguration.getTemplate().getExpansionUrl() + "/getTemplateList");
            CloseableHttpResponse response = httpClient.execute(request);
            if (HttpStatus.SC_OK != response.getStatusLine().getStatusCode()) {
                logger.error("Failed to retrieve template list: " + response.getStatusLine().toString());
                return null;
            }
            HttpEntity entity = response.getEntity();
            String body = EntityUtils.toString(response.getEntity());
            EntityUtils.consume(entity);
            return new JSONArray(body);
        } catch (Exception ex) {
            logger.error("Could not obtain template list from provenance catalog at "+
                    provenanceConfiguration.getTemplate().getExpansionUrl() + "/getTemplateList");
        }
        return null;
    }
}
