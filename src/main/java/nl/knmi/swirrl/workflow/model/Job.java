package nl.knmi.swirrl.workflow.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 * Job
 */
public class Job   {
    @JsonProperty("id")
    private String id = null;

    @JsonProperty("status")
    private String status = null;

    @JsonProperty("logs")
    private byte[] logs = null;

    @JsonProperty("startTime")
    private String startTime = null;

    @JsonProperty("endTime")
    private String endTime = null;

    @JsonProperty("imageID")
    private String imageID = null;

    public Job id(String id) {
        this.id = id;
        return this;
    }

    /**
     * ID of a running or completed (Kubernetes) Job.
     * @return id
     **/
    @ApiModelProperty(value = "ID of a running or completed (Kubernetes) Job.")


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setImageID(String imageID) {
        this.imageID = imageID;
    }

    public Job status(String status) {
        this.status = status;
        return this;
    }

    /**
     * status of the job.
     * @return status
     **/
    @ApiModelProperty(value = "status of the job.")


    public String getImageID() {
        return this.imageID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Job logs(byte[] logs) {
        this.logs = logs;
        return this;
    }

    /**
     * base64 encoded logs of the job's pod. Decode with:  jq -M '.[\"logs\"]' response_file.json | sed -e 's/\"//g' | base64 -d -
     * @return logs
     **/
    @ApiModelProperty(value = "base64 encoded logs of the job's pod. Decode with:  jq -M '.[\"logs\"]' response_file.json | sed -e 's/\"//g' | base64 -d - ")


    public byte[] getLogs() {
        return logs;
    }

    public void setLogs(byte[] logs) {
        this.logs = logs;
    }

    public Job startTime(String startTime) {
        this.startTime = startTime;
        return this;
    }

    /**
     * Time at which the job was started.
     * @return startTime
     **/
    @ApiModelProperty(value = "Time at which the job was started.")


    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public Job endTime(String endTime) {
        this.endTime = endTime;
        return this;
    }

    /**
     * Time at which the job ended.
     * @return endTime
     **/
    @ApiModelProperty(value = "Time at which the job ended.")


    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Job job = (Job) o;
        return Objects.equals(this.id, job.id) &&
                Objects.equals(this.status, job.status) &&
                Objects.equals(this.logs, job.logs) &&
                Objects.equals(this.startTime, job.startTime) &&
                Objects.equals(this.endTime, job.endTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, status, logs, startTime, endTime);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Job {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    status: ").append(toIndentedString(status)).append("\n");
        sb.append("    logs: ").append(toIndentedString(logs)).append("\n");
        sb.append("    startTime: ").append(toIndentedString(startTime)).append("\n");
        sb.append("    endTime: ").append(toIndentedString(endTime)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

