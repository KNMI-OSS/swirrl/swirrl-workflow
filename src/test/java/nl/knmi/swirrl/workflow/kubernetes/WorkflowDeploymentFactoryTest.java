package nl.knmi.swirrl.workflow.kubernetes;

import io.kubernetes.client.openapi.models.V1ConfigMap;
import io.kubernetes.client.openapi.models.V1Job;
import io.kubernetes.client.util.Yaml;
import org.junit.jupiter.api.Test;
import org.openapitools.v1_0.model.ActiveWorkflow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.TestPropertySource;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static net.javacrumbs.jsonunit.assertj.JsonAssertions.assertThatJson;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@TestPropertySource(properties = {
        "workflow.docker.image.pull.secret=secretname"
})
public class WorkflowDeploymentFactoryTest {

    @Autowired
    WorkflowDeploymentFactory workflowDeploymentFactory;

    @Test
    public void makeDownloadWorkflowDeploymentTest() throws IOException {
        byte[] inputs = this.getClass().getClassLoader().getResourceAsStream("workflow/download-inputs.yaml").readAllBytes();
        ActiveWorkflow workflow = new ActiveWorkflow("test-session-id", inputs, "test-run-id",
                "download", ActiveWorkflow.RUN_ACTIVITY_COMMAND, null, "mock_user_id@swirrl-api",
                "Mock User", "mock_auth_mode", "mock_group", "SWIRRL-API");

        String datadirPVCName = "data-directory-test-session-id";
        V1ConfigMap expectedInputsConfigMap = Yaml.loadAs(
                new InputStreamReader(
                        new ClassPathResource("workflow/expected-download-inputs-configmap.yaml").getInputStream()), V1ConfigMap.class);
        expectedInputsConfigMap.putDataItem("metadata.txt", "test-run-id");
        V1ConfigMap expectedWorkflowConfigMap = Yaml.loadAs(
                new InputStreamReader(
                        new ClassPathResource("workflow/expected-download-workflow-configmap.yaml").getInputStream()), V1ConfigMap.class);
        V1Job expectedJob = Yaml.loadAs(
                new InputStreamReader(
                        new ClassPathResource("workflow/expected-download-job.yaml").getInputStream()), V1Job.class
        );

        WorkflowDeployment deployment = workflowDeploymentFactory.makeWorkflowDeployment("download", workflow);

        assertThat(deployment.getInputsConfigMap()).isEqualTo(expectedInputsConfigMap);
        assertThat(deployment.getDatadirPvcName()).isEqualTo(datadirPVCName);
        assertThatJson(deployment.getJob()).isEqualTo(expectedJob);
    }

    @Test
    public void makeOpendapWorkflowDeploymentTest() throws IOException {
        byte[] inputs = this.getClass().getClassLoader().getResourceAsStream("workflow/opendap-inputs-time.yaml").readAllBytes();
        ActiveWorkflow workflow = new ActiveWorkflow("test-session-id", inputs, "test-run-id",
                "opendap",ActiveWorkflow.RUN_ACTIVITY_COMMAND, null, "mock_user_id@swirrl-api",
                "Mock User", "mock_auth_mode", "mock_group", "SWIRRL-API");

        String datadirPVCName = "data-directory-test-session-id";
        V1ConfigMap expectedInputsConfigMap = Yaml.loadAs(
                new InputStreamReader(
                        new ClassPathResource("workflow/expected-opendap-inputs-time-configmap.yaml").getInputStream()), V1ConfigMap.class);
        expectedInputsConfigMap.putDataItem("metadata.txt", "test-run-id");
        V1ConfigMap expectedWorkflowConfigMap = Yaml.loadAs(
                new InputStreamReader(
                        new ClassPathResource("workflow/expected-opendap-workflow-configmap.yaml").getInputStream()), V1ConfigMap.class);
        V1Job expectedJob = Yaml.loadAs(
                new InputStreamReader(
                        new ClassPathResource("workflow/expected-opendap-job.yaml").getInputStream()), V1Job.class
        );

        WorkflowDeployment deployment = workflowDeploymentFactory.makeWorkflowDeployment("opendap", workflow);

        assertThat(deployment.getInputsConfigMap()).isEqualTo(expectedInputsConfigMap);
        assertThat(deployment.getDatadirPvcName()).isEqualTo(datadirPVCName);
        assertThatJson(deployment.getJob()).isEqualTo(expectedJob);
    }
}
