package nl.knmi.swirrl.workflow.provenance;

import io.kubernetes.client.openapi.models.V1Pod;
import nl.knmi.swirrl.workflow.SwirrlApiConfiguration;
import nl.knmi.swirrl.workflow.TemplateCatalog;
import nl.knmi.swirrl.workflow.kubernetes.KubernetesService;
import nl.knmi.swirrl.workflow.kubernetes.WorkflowDeploymentFactory;
import nl.knmi.swirrl.workflow.model.Job;
import org.joda.time.DateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openapitools.v1_0.model.ActiveWorkflow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import static net.javacrumbs.jsonunit.assertj.JsonAssertions.assertThatJson;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;

import org.mockito.*;

import static org.mockito.Mockito.*;

@SpringBootTest
@EnableConfigurationProperties(value = {ProvenanceConfiguration.class, SwirrlApiConfiguration.class})
public class WorkflowProvenanceServiceTest {

    @Mock
    private KubernetesService kubernetesServiceMock;

    @Autowired
    private ProvenanceConfiguration provenanceConfiguration;

    @Autowired
    private SwirrlApiConfiguration swirrlApiConfiguration;

    @Mock
    private TemplateCatalog templateCatalogMock;

    private WorkflowProvenanceService provenanceService;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private V1Pod podMock;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.provenanceService = new WorkflowProvenanceService(
                kubernetesServiceMock, provenanceConfiguration, swirrlApiConfiguration, templateCatalogMock);
    }

    @Test
    public void traceProvenanceTest() throws Exception {
        System.out.println(String.format("these are my properties: \n%s", provenanceConfiguration));
        byte[] inputs = this.getClass().getClassLoader().getResourceAsStream("workflow/download-inputs.yaml").readAllBytes();
        ActiveWorkflow workflow = new ActiveWorkflow("test-session-id", inputs, "test-run-id",
                "download", ActiveWorkflow.RUN_ACTIVITY_COMMAND, null, "mock_user_id@swirrl-api",
                "mock_user_id", "mock_auth_mode", "mock_group", "SWIRRL-API");
        Job job = new Job();
        job.setId("test-run-id");
        job.setImageID("docker-pullable://swirrl-workflow@sha256:123456789abcdef");
        job.setStartTime(DateTime.now().toString());
        job.setEndTime(DateTime.now().toString());
        job.setStatus("Succeeded");

        byte[] jobLogs = this.getClass().getClassLoader().getResourceAsStream("workflow/download-logs.log").readAllBytes();
        job.setLogs(jobLogs);

        String expectedProvenanceBindings = new String(Files.readAllBytes(
                Paths.get(getClass().getClassLoader().getResource("workflow/expected-download-bindings.json").toURI())));

        when(kubernetesServiceMock.retrieveLatestJobPod(WorkflowDeploymentFactory.RUN_ID_LABEL, job.getId())).thenReturn(podMock);
        when(podMock.getStatus().getContainerStatuses().get(0).getImageID())
                .thenReturn("docker-pullable://swirrl-workflow@sha256:123456789abcdef");

        String provenanceBindings = this.provenanceService.traceProvenance(workflow, job);
        System.out.println("Provenance bindings:\n"+provenanceBindings);
        System.out.println("Expected provenance bindings:\n"+expectedProvenanceBindings);
        assertThatJson(provenanceBindings).isEqualTo(expectedProvenanceBindings);
    }

    @Test
    public void traceProvenanceNullEntityTest() throws Exception {
        System.out.println(String.format("these are my properties: \n%s", provenanceConfiguration));
        byte[] inputs = this.getClass().getClassLoader().getResourceAsStream("workflow/download-inputs.yaml").readAllBytes();
        ActiveWorkflow workflow = new ActiveWorkflow("test-session-id", inputs, "test-run-id",
                "download", ActiveWorkflow.RUN_ACTIVITY_COMMAND, null, "mock_user_id@swirrl-api",
                "mock_user_id", "mock_auth_mode", "mock_group", "SWIRRL-API");
        Job job = new Job();
        job.setId("test-run-id");
        job.setImageID("docker-pullable://swirrl-workflow@sha256:123456789abcdef");
        job.setStartTime(DateTime.now().toString());
        job.setEndTime(DateTime.now().toString());
        job.setStatus("Succeeded");

        byte[] jobLogs = this.getClass().getClassLoader().getResourceAsStream("workflow/rookwps.log").readAllBytes();
        job.setLogs(jobLogs);
        this.provenanceService.traceProvenance(workflow, job);
    }

}
